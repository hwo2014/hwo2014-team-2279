#ifndef TSC_ALLOC_H
#define TSC_ALLOC_H

#include "bit.h"

namespace tsc {
;

// This allocator is NOT thread-safe.

// namespace native_allocator {
// 	extern "C" void* __stdcall VirtualAlloc(void*address,size_t size,unsigned long type,unsigned long protect);
// 	extern "C" int __stdcall VirtualFree(void*address,size_t size,unsigned long type);
// }
/*
namespace alloc_impl {
	size_t bytes_allocated = 0;
	size_t max_bytes_allocated = 0;
	void*raw_allocate(size_t n) {
		bytes_allocated += n;
		if (bytes_allocated>max_bytes_allocated) max_bytes_allocated = bytes_allocated;
		return malloc(n);
	}
	void raw_deallocate(void*ptr,size_t n) {
		bytes_allocated -= n;
		return free(ptr);
	}

	template<size_t object_size,size_t object_alignment>
	struct allocator {

		template<size_t v,int count=0>
		struct templ_count_leading_zeros {
			static const size_t shift_value = (((size_t)1)<<(sizeof(size_t)*8-1-count));
			static const size_t next_value = templ_count_leading_zeros<v,count+1>::value;
			static const size_t value = v&shift_value ? count : next_value;
		};
		template<size_t v>
		struct templ_count_leading_zeros<v,sizeof(size_t)*8> {
			static const size_t value = sizeof(size_t)*8;
		};
		template<size_t... v>
		struct test {
			
		};

		typedef unsigned long avail_t;
		static const size_t bits_per_avail_t = sizeof(avail_t)*8;
		static const size_t avail_n = 13;
		struct chunk {
			int idx;
			void*memory;
			std::pair<chunk*,chunk*> link;
			int avail_count;
			std::array<avail_t,avail_n> avail;
			chunk();
			~chunk();
		};

		static const size_t objects_per_chunk = bits_per_avail_t*avail_n;

		static const size_t object_size = object_size;
		static const size_t max_a_consecutive_chunks = 1024ul*1024*4/(objects_per_chunk*object_size);
		static const size_t max_shift = max_a_consecutive_chunks==0 ? 0 : sizeof(size_t)*8-1 - templ_count_leading_zeros<max_a_consecutive_chunks>::value;
		//static const size_t data_alignment = data_size-1;
		static const size_t min_bytes = object_alignment<8 ? 8 : object_alignment;
		static const size_t min_shift = sizeof(size_t)*8 - templ_count_leading_zeros<min_bytes/object_size ? (min_bytes/object_size)-1 : 0>::value;
		//static const size_t t = test<min_shift,max_shift>::value;
		static const size_t min_n = 1ul<<min_shift;
		static const size_t max_n = 1ul<<max_shift;
		//static const size_t t = test<min_shift,max_shift,min_n,max_n>::value;

// 		static const size_t max_chunk_size_in_bytes = max_n * sizeof(chunk);
// 		static const size_t max_data_size_in_bytes = max_n * objects_per_chunk * object_size;
		
		static const size_t max_idx = max_shift-min_shift;
		static const size_t chunks_per_chunk_size = 1ul<<max_idx;
		static const size_t chunks_size = sizeof(chunk) << max_idx;
		static const size_t data_size = (objects_per_chunk*object_size) << max_shift;

		static const size_t chunk_map_granularity = 1024ul;
		//static const size_t t = test<object_size,objects_per_chunk,chunks_per_map_entry>::value;
		typedef std::array<chunk*,chunk_map_granularity> chunk_entry_t;
		std::array<chunk_entry_t*,~(size_t)0 / chunk_map_granularity + 1> chunk_map;

		std::array<tsc::intrusive_list<chunk,&chunk::link>,max_idx+1> lists;
		allocator() : lists() {}

		//std::vector<std::tuple<void*,chunk*,size_t>> live_memory;

		chunk*allocate_new_chunk() {

			chunk*c = (chunk*)raw_allocate(chunks_size);
			if (!c) throw std::bad_alloc();
			//printf("allocator<%d,%d>: allocated %d chunks at [%p,%p) (%d bytes)\n",object_size,object_alignment,chunks_per_chunk_size,c,c + chunks_per_chunk_size,chunks_size);
			const size_t memory_alignment = objects_per_chunk * object_size;
			const size_t memory_size = data_size + memory_alignment;
			char*memory = (char*)raw_allocate(memory_size);
			if (!memory) {
				raw_deallocate(c,chunks_size);
				throw std::bad_alloc();
			}
			if ((intptr_t)memory % memory_alignment) memory += memory_alignment - (intptr_t)memory % memory_alignment;
			//printf("allocator<%d,%d>: allocated memory at [%p,%p) (%d bytes, %d alignment)\n",object_size,object_alignment,memory,memory + data_size,data_size,memory_alignment);
			for (size_t i=0;i<chunks_per_chunk_size;++i) {
				c[i].avail.fill(~(avail_t)0);
				c[i].avail_count = objects_per_chunk;
				c[i].idx = -1;
				c[i].memory = &memory[i * (objects_per_chunk * object_size)];
				chunk_entry_t*&ce = chunk_map[(intptr_t)c[i].memory / (objects_per_chunk*object_size) / chunk_map_granularity];
				if (!ce) {
					ce = (chunk_entry_t*)raw_allocate(sizeof(chunk_entry_t));
					if (!ce) {
						raw_deallocate(memory,memory_size);
						raw_deallocate(c,chunks_size);
						throw std::bad_alloc();
					}
					memset(ce,0,sizeof(chunk_entry_t));
					//printf("allocator<%d,%d>: allocated new chunk map for memory at %p (%d bytes)\n",object_size,object_alignment,c[i].memory,sizeof(chunk_entry_t));
				}
				//printf("(intptr_t)c[i].memory %% chunk_map_granularity is %d\n",(intptr_t)c[i].memory % chunk_map_granularity);
				//printf("(intptr_t)c[i].memory %% chunk_map_granularity / (objects_per_chunk * object_size) is %d\n",(intptr_t)c[i].memory % chunk_map_granularity / (objects_per_chunk * object_size));
				//printf("objects_per_chunk is %d, object_size is %d\n",objects_per_chunk,object_size);
				//printf("c[%d].memory is %p\n",i,c[i].memory);
// 				chunk*&cptr = (*ce)[(intptr_t)c[i].memory % chunk_map_granularity / (objects_per_chunk * object_size)];
// 				if (cptr) xcept("cptr is already set");
// 				cptr = &c[i];
			}
// 			for (auto&v : live_memory) {
// 				void*ptr;
// 				chunk*c;
// 				std::tie(ptr,c) = v;
// 				if ((*chunk_map[(intptr_t)ptr / chunk_map_granularity])[(intptr_t)ptr % chunk_map_granularity / (objects_per_chunk * object_size)]!=c) {
// 					printf(":(\n");
// 					DebugBreak();
// 				}
// 			}
			c->idx = max_idx;
			//lists[max_idx].push_back(*c);
			//printf("allocator<%d,%d>: allocated new chunk at %p with memory at %p\n",object_size,object_alignment,c,memory);
			return c;
		}

		chunk*get_new_chunk(size_t idx) {

			size_t cur_idx = idx;
			chunk*c = 0;
			while (true) {
				if (cur_idx==max_idx) {
					c = allocate_new_chunk();
					break;
				}
				++cur_idx;
				auto&l = lists[cur_idx];
				if (!l.empty()) {
					chunk*h = &l.back();
					if (h->avail_count==objects_per_chunk) {
						l.erase(*h);
						c = h;
						//printf("allocator<%d,%d>: found unused chunk %p with index %d\n",object_size,object_alignment,c,cur_idx);
						break;
					}
				}
			}
			bool is_last = ~c->idx&32 ? true : false;
			c->idx |= 32;
			lists[idx].push_back(*c);
			while (cur_idx!=idx) {
				--cur_idx;
				chunk*n = c + (1<<cur_idx);
				if (is_last) n->idx = cur_idx, is_last = false;
				else n->idx = cur_idx | 32;
				lists[cur_idx].push_back(*n);
				//printf("allocator<%d>,%d: split chunk %p, add chunk %p to index %d%s\n",object_size,object_alignment,c,n,cur_idx,~n->idx&32 ? " (last chunk)" : "");
// 				if ((*chunk_map[(intptr_t)n->memory / chunk_map_granularity])[(intptr_t)n->memory % chunk_map_granularity / (objects_per_chunk * object_size)]!=n) {
// 					printf(" :(((\n");
// 					DebugBreak();
// 				}
			}
// 			if ((*chunk_map[(intptr_t)c->memory / chunk_map_granularity])[(intptr_t)c->memory % chunk_map_granularity / (objects_per_chunk * object_size)]!=c) {
// 				printf(" :((\n");
// 				DebugBreak();
// 			}
			if (is_last) c->idx = idx;
			else c->idx = idx | 32;
			//printf("allocator<%d,%d>: get_new_chunk(%d) returning %p\n",object_size,object_alignment,idx,c);
			return c;
		}

		void merge_chunk_if_possible(chunk*c,size_t idx) {
			if (idx==max_idx) return;
			if (~c->idx&32) return;
			chunk*n = c + (1<<idx);
			if ((n->idx&31)!=idx) return;
			if (n->avail_count!=objects_per_chunk) return;
			lists[idx].erase(*c);
			++idx;
			//printf("allocator<%d,%d>: merge chunk %p to idx %d\n",object_size,object_alignment,c,idx);
			if (n->idx&32) c->idx = idx | 32;
			else c->idx = idx;
			lists[idx].push_back(*c);
			merge_chunk_if_possible(c,idx);
		}

		static const size_t max_avail_idx = objects_per_chunk-1;
		template<typename T>
		size_t extract_avail_idx(T&cont) {
// 			size_t n = cont.size();
// 			size_t rn = 0;
// 			while (n>0) {
// 				int n2 = n/2;
// 				if (cont[rn + n2]) n = n2;
// 				else {
// 					rn = rn+n2+1;
// 					n -= n2 + 1;
// 				}
// 			}
			size_t rn = 0;
			while (cont[rn]==0) ++rn;
			auto&v = cont[rn];
			//printf("allocator<%d,%d>: extract_avail_idx: rn is %d, v is %lx\n",object_size,object_alignment,rn,v);
			if (!v) DebugBreak();
			size_t idx = bit_count_trailing_zeros(v);
			v &= ~1ul<<idx;
			size_t r = rn*bits_per_avail_t + idx;
			//printf("allocator<%d,%d>: extract_avail_idx: bit index is %d, returning %d\n",object_size,object_alignment,idx,r);
			return r;
		}

		void*allocate(size_t n) {
			//printf("allocator<%d,%d>: allocate %d, min_shift %d, max_shift %d\n",object_size,object_alignment,n,min_shift,max_shift);
			if (max_shift==min_shift) return raw_allocate(n*object_size);
			size_t shift  = sizeof(n)*8-bit_count_leading_zeros(n-1);
			if (1ull<<shift<n) xcept("wtf");
			size_t idx = shift - min_shift;
			if (idx>max_idx) return raw_allocate(n*object_size);
			chunk*c = !lists[idx].empty() ? &lists[idx].front() : get_new_chunk(idx);
			size_t avail_idx = extract_avail_idx(c->avail);
			--c->avail_count;
			if (avail_idx==max_avail_idx) {
				lists[idx].erase(*c);
			}
			void*r = (char*)c->memory + (object_size<<shift)*avail_idx;
			chunk*&cptr = (*chunk_map[(intptr_t)r / (objects_per_chunk*object_size) / chunk_map_granularity])[(intptr_t)r / (objects_per_chunk*object_size) % chunk_map_granularity];
			cptr = c;
			//live_memory.emplace_back(r,c,idx);
			//printf("allocator<%d,%d>: allocate(%d) returning chunk %p with idx %d, avail_idx %d, memory at %p\n",object_size,object_alignment,n,c,idx,avail_idx,r);
// 			for (auto&v : live_memory) {
// 				void*ptr;
// 				chunk*c;
// 				size_t idx;
// 				std::tie(ptr,c,idx) = v;
// 				//ptr = (void*)((intptr_t)ptr / (object_size<<(min_shift + idx)) * (object_size<<(min_shift + idx)));
// 				printf("ptr is %p, idx is %d\n",ptr,idx);
// 				if ((*chunk_map[(intptr_t)ptr / chunk_map_granularity])[(intptr_t)ptr % chunk_map_granularity / (objects_per_chunk * object_size)]!=c) {
// 					printf("moo :(\n");
// 					DebugBreak();
// 				}
// 			}
			return r;
		}
		void deallocate(void*ptr,size_t n) {
			if (max_shift==min_shift) return raw_deallocate(ptr,n*object_size);
			size_t shift  = sizeof(n)*8-bit_count_leading_zeros(n-1);
			size_t idx = shift - min_shift;
			if (idx>max_idx) return raw_deallocate(ptr,n*object_size);
			//printf("allocator<%d,%d>: deallocate %p %d\n",object_size,object_alignment,ptr,n);
			chunk*&cptr = (*chunk_map[(intptr_t)ptr / (objects_per_chunk*object_size) / chunk_map_granularity])[(intptr_t)ptr / (objects_per_chunk*object_size) % chunk_map_granularity];
			chunk*c = cptr;
			//cptr = 0;
			size_t avail_idx = ((intptr_t)ptr - (intptr_t)c->memory) / (object_size<<shift);
			//printf("allocator<%d,%d>: deallocate %d at %p, chunk is %p, avail_idx is %d\n",object_size,object_alignment,n,ptr,c,avail_idx);
			if (((intptr_t)ptr - (intptr_t)c->memory) & ((1ull<<shift)-1)) {
				printf("bad deallocate\n");
				fflush(stdout);
				DebugBreak();
			}
			if ((c->idx&31)!=idx) {
				printf("bad chunk idx %d should be %d\n",c->idx&31,idx);
				DebugBreak();
			}
			c->avail[avail_idx / bits_per_avail_t] |= 1ul<<(avail_idx%bits_per_avail_t);
			++c->avail_count;
			if (c->avail_count==1) {
				lists[idx].push_front(*c);
			} else if (c->avail_count==objects_per_chunk) {
				merge_chunk_if_possible(c,idx);
			}
// 			for (auto i=live_memory.begin();i!=live_memory.end();++i) {
// 				if (*i==std::make_tuple(ptr,c,idx)) {
// 					live_memory.erase(i);
// 					break;
// 				}
// 			}
		}
		
		static allocator&get() {
			static allocator a;
			return a;
		}
	};

	template<typename T>
	T*allocate(size_t n) {
		return (T*)allocator<sizeof(T),std::alignment_of<T>::value>::get().allocate(n);
	}
	template<typename T>
	void deallocate(T*ptr,size_t n) {
		allocator<sizeof(T),std::alignment_of<T>::value>::get().deallocate(ptr,n);
	}
// 
// 	void*allocate_old(size_t on) {
// 		size_t n = on + sizeof(base);
// 		size_t idx = sizeof(n)*8-bit_count_leading_zeros(n>>3);
// 		if (idx>=lists.size()) return ++extra_allocations, raw_allocate(n);
// 		base*&head = lists[idx];
// 		if (!head) return ++extra_allocations, ((base*)raw_allocate(8<<idx))+1;
// 		++allocations[idx];
// 		base*r = head;
// 		head = r->next;
// 		return r+1;
// 	}
// 	void deallocate_old(void*ptr,size_t on) {
// 		size_t n = on + sizeof(base);
// 		size_t idx = sizeof(n)*8-bit_count_leading_zeros(n>>3);
// 		if (idx>=lists.size()) return raw_deallocate(ptr,n);
// 		base*&head = lists[idx];
// 		base*newhead = ((base*)ptr)-1;
// 		newhead->next = head;
// 		head = newhead;
// 	}

}*/

namespace alloc_impl {
	size_t bytes_allocated = 0;
	size_t max_bytes_allocated = 0;
	void*raw_allocate(size_t n) {
		bytes_allocated += n;
		if (bytes_allocated>max_bytes_allocated) max_bytes_allocated = bytes_allocated;
		return malloc(n);
	}
	void raw_deallocate(void*ptr,size_t n) {
		bytes_allocated -= n;
		return free(ptr);
	}

	struct base {
		base*next;
		base();
		~base();
	};

	std::array<base*,8> lists{};
	std::array<int,8> allocations{};
	int extra_allocations = 0;

	void*allocate(size_t on) {
		size_t n = on + sizeof(base);
		size_t idx = sizeof(n)*8-bit_count_leading_zeros(n>>3);
		if (idx>=lists.size()) return ++extra_allocations, raw_allocate(n);
		base*&head = lists[idx];
		if (!head) return ++extra_allocations, ((base*)raw_allocate(8<<idx))+1;
		++allocations[idx];
		base*r = head;
		head = r->next;
		return r+1;
	}
	void deallocate(void*ptr,size_t on) {
		size_t n = on + sizeof(base);
		size_t idx = sizeof(n)*8-bit_count_leading_zeros(n>>3);
		if (idx>=lists.size()) return raw_deallocate(ptr,n);
		base*&head = lists[idx];
		base*newhead = ((base*)ptr)-1;
		newhead->next = head;
		head = newhead;
	}

}


template<typename T> struct alloc;
template<>
struct alloc<void> {
	typedef void value_type;
	typedef void*pointer;
	typedef const void*const_pointer;
	typedef size_t size_type;
	typedef ptrdiff_t difference_type;
	template<typename t>
	struct rebind {
		typedef alloc<t> other;
	};
};
template<typename T>
struct alloc {
	typedef T value_type;
	typedef T*pointer;
	typedef const T*const_pointer;
	typedef T&reference;
	typedef const T&const_reference;
	typedef size_t size_type;
	typedef ptrdiff_t difference_type;
	template<typename t>
	struct rebind {
		typedef alloc<t> other;
	};
	alloc() {}
	alloc(const alloc&other) {}
	template<typename t>
	alloc(const alloc<t>&other) {}
	~alloc() {}
	pointer allocate (size_type n,alloc<void>::const_pointer hint=0) {
		return (T*)alloc_impl::allocate(sizeof(T)*n);
		//return alloc_impl::allocate<T>(n);
	}
	void deallocate(pointer p,size_type n) {
		//alloc_impl::deallocate<T>(p,n);
		alloc_impl::deallocate(p,sizeof(T)*n);
	}
	alloc&operator=(const alloc&n) {
		return *this;
	}
	alloc&operator=(alloc&&n) {
		return *this;
	}
	bool operator==(const alloc&n) const {
		return true;
	}
	bool operator!=(const alloc&n) const {
		return false;
	}
	template<class U, class... Args>
	void construct(U*p,Args&&... args) {
		::new ((void*)p) U(std::forward<Args>(args)...);
	}
	template<class U>
	void destroy(U*p) {
		p->~U();
	}
	size_type max_size() const {
		return std::numeric_limits<size_type>::max();
	}
// 	pointer address(reference v) {
// 		return &v;
// 	}
// 	const_pointer address(const_reference v) {
// 		return &v;
// 	}
};

}

#endif
