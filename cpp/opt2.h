
namespace opt2 {
;

#include "fast_lane.h"

simul sim;

double turbo_factor;
int turbo_ticks;
int my_last_switch;
int turbo_available_in;

a_vector<a_vector<action>> find_actions;
a_vector<a_vector<simul::state>> find_states;
size_t find_advance_count;

double best_turbo_difference;

int find_max_ticks = 60*4;

a_vector<double> default_throttle_for_car;

void find() {

	double track_length = get_abs_center_piece_pos(race.track.pieces.size()-1) + get_center_piece_length(race.track.pieces.size()-1);

	a_vector<double> race_positions;

	a_vector<std::pair<double,size_t>> standings;

	a_vector<simul::state> initial_states;
	for (size_t i=0;i<st.cars.size();++i) {
		auto&v = st.cars[i];
		simul::state is;
		is.throttle = v.last_throttle;
		is.piece_index = v.piece_index;
		is.piece_pos = v.piece_pos;
		is.speed = v.speed;
		is.rot = v.rot;
		is.angle = v.angle;
		is.start_lane = v.start_lane;
		is.end_lane = v.end_lane;
		is.rotacc = 0;
		is.last_switch = i==my_car_index ? my_last_switch : 0;
		is.turbo_ticks = v.turbo_ticks;
		is.has_turbo = v.has_turbo;
		is.turbo_next_tick = v.turbo_next_tick;
		is.tick = 0;
		is.crashed_ticks = std::max(v.crashed_ticks - 1,0);
		is.laps_finished = v.laps_finished;
		is.lap = v.lap;
		is.next_desired_lane = -1;
		initial_states.push_back(is);

		double pos = track_length * is.lap;
		pos += get_abs_center_piece_pos(is.piece_index);
		pos += (is.piece_pos / get_piece_length(is.piece_index,is.start_lane,is.end_lane)) * get_center_piece_length(is.piece_index);

		log(log_level_info,"race_position for %d is %f\n",i,pos);
		race_positions.push_back(pos);
		standings.emplace_back(pos,i);
	}
	std::sort(standings.begin(),standings.end(),std::greater<std::pair<double,size_t>>());

	a_vector<double> traffic_factors;
	traffic_factors.resize(race.track.lanes.size(),1.0);
	for (size_t i=0;i<initial_states.size();++i) {
		auto&is = initial_states[i];
		if (is.crashed_ticks) continue;
		auto&my_is = initial_states[my_car_index];
		double my_pos = get_abs_center_piece_pos(my_is.piece_index) + (my_is.piece_pos / get_piece_length(my_is.piece_index,is.start_lane,my_is.end_lane)) * get_center_piece_length(my_is.piece_index);
		double n_pos = get_abs_center_piece_pos(is.piece_index) + (is.piece_pos / get_piece_length(is.piece_index,is.start_lane,is.end_lane)) * get_center_piece_length(is.piece_index);

		if (n_pos>my_pos && n_pos-my_pos<200.0) {
			if (default_throttle_for_car.size()>i) traffic_factors.at(is.end_lane) *= default_throttle_for_car[i];
		}

	}
	log("traffic: ");
	for (auto&v : traffic_factors) log(" %f",v);
	log("\n");

	size_t my_standing = 0;
	for (size_t i=0;i<standings.size();++i) {
		if (standings[i].second==my_car_index) my_standing = i;
	}

	a_vector<a_vector<action>> actions;
	a_vector<a_vector<simul::state>> states;

	actions.resize(st.cars.size());
	states.resize(st.cars.size());
	for (size_t i=0;i<st.cars.size();++i) {
		actions[i].reserve(65 * 4);
		states[i].reserve(65 * 4);

		actions[i].push_back(action());
		states[i].push_back(initial_states[i]);
	}

	size_t advance_count = 0;

	a_vector<a_vector<a_vector<action>>> evil_throttle_actions;
	a_vector<a_vector<a_vector<simul::state>>> evil_throttle_states;
	evil_throttle_actions.resize(st.cars.size());
	evil_throttle_states.resize(st.cars.size());
	for (size_t idx=0;idx<st.cars.size();++idx) {
		evil_throttle_actions[idx].emplace_back();
		evil_throttle_states[idx].emplace_back();
		auto&cur_actions = evil_throttle_actions[idx].back();
		auto&cur_states = evil_throttle_states[idx].back();
		cur_actions.push_back(action());
		cur_states.push_back(initial_states[idx]);
		//if (idx==my_car_index && cur_states.back().last_switch) continue;
		for (size_t i=0;i<180;++i) {
			cur_states.push_back(cur_states.back());
			auto&st = cur_states.back();
			action a(action::t_throttle,1.0);
			if (st.has_turbo && !st.turbo_ticks) a.t = action::t_turbo;
			cur_actions.push_back(a);
			++advance_count;
			sim.advance(st,a);
			if (st.crashed_ticks) break;
		}
		{
			evil_throttle_actions[idx].emplace_back();
			evil_throttle_states[idx].emplace_back();
			auto&cur_actions = evil_throttle_actions[idx].back();
			auto&cur_states = evil_throttle_states[idx].back();
			cur_actions.push_back(action());
			cur_states.push_back(initial_states[idx]);
			for (size_t i=0;i<15;++i) {
				cur_states.push_back(cur_states.back());
				auto&st = cur_states.back();
				action a(action::t_throttle,0.0);
				if (st.has_turbo && !st.turbo_ticks) a.t = action::t_turbo;
				cur_actions.push_back(a);
				++advance_count;
				sim.advance(st,a);
				if (st.crashed_ticks) break;
			}
			for (size_t i=0;i<30;++i) {
				cur_states.push_back(cur_states.back());
				auto&st = cur_states.back();
				action a(action::t_throttle,1.0);
				if (st.has_turbo && !st.turbo_ticks) a.t = action::t_turbo;
				cur_actions.push_back(a);
				++advance_count;
				sim.advance(st,a);
				if (st.crashed_ticks) break;
			}
		}
	}

	a_vector<std::tuple<size_t,size_t,size_t,size_t>> evil_bumps;

	bool my_bump_is_crash = true;

	bool eval_force_turbo = false;
	bool eval_override_laps = false;
	bool eval_override_ticks = false;
	int eval_laps = 0;
	int eval_ticks = 0;
	auto eval = [&](size_t cur_idx) {
		log("eval %d -\n",cur_idx);
		auto&cur_actions = actions[cur_idx];
		auto&cur_states = states[cur_idx];
		size_t old_size = cur_states.size();
		//for (size_t i=0;i<8;++i) {
		for (size_t i=0;i<60;++i) {

			{
				auto&last_st = cur_states.back();
				//if (last_st.laps>=3) return true;
				//if (last_st.laps>=1) return true;
				//if (last_st.tick>=60*60) return true;
				//if (last_st.tick+last_st.crashed_ticks>=60*60) return true;
				if (eval_override_ticks) {
					if (last_st.tick>=eval_ticks) return true;
				} else if (eval_override_laps) {
					if (last_st.laps>=eval_laps) return true;
					if (last_st.tick>=60*15) return true;
				} else {
					if (last_st.tick>=find_max_ticks) return true;
					//if (last_st.tick>=60*4) return true;
				}
				if (race.laps && last_st.laps_finished>=race.laps) return true;
			}

// 			if (false) {
// 				auto&last_st = cur_states.back();
// 
// 				if (!last_st.crashed_ticks && last_st.bumps.any() && cur_states.size()>60) {
// 					auto bumps = cur_states[cur_states.size()-60].bumps;
// 					for (size_t i=59;i>=30;--i) {
// 						if (i>=cur_states.size()) break;
// 						size_t idx = cur_states.size() - i;
// 						auto&st = cur_states[idx];
// 						if (st.bumps.none()) break;
// 						for (size_t idx : bumps) {
// 							if (!bumps.test(idx)) {
// 								log("survived bump with %d at tick %d\n",idx,st.tick);
// 								cur_states.resize(idx);
// 								cur_actions.resize(idx);
// 								cur_states[idx-1].bumps.set(idx);
// 							}
// 						}
// 					}
// 
// 
// 				}
// 
// 			}

			size_t pad_n = 0;
			if (cur_actions.capacity()<cur_actions.size()+1+pad_n) cur_actions.reserve(cur_actions.size()*2 + 1 + pad_n);
			if (cur_states.capacity()<cur_states.size()+1+pad_n) cur_states.reserve(cur_states.size()*2 + 1 + pad_n);

			double default_throttle = 1.0;
			if (default_throttle_for_car.size()>cur_idx) default_throttle = default_throttle_for_car[cur_idx];
			
			cur_actions.emplace_back(action::t_throttle,default_throttle);
			cur_states.push_back(cur_states.back());
			auto&last_st = cur_states[cur_states.size()-2];
			auto&a = cur_actions.back();
			auto&st = cur_states.back();

			if (st.crashed_ticks) st.bumps.reset();

			size_t next_piece_index = st.piece_index==race.track.pieces.size()-1 ? 0 : st.piece_index+1;
			size_t next_next_piece_index = next_piece_index==race.track.pieces.size()-1 ? 0 : next_piece_index+1;
			size_t next_fast_lane = fast_lane_for_piece[next_next_piece_index];
			size_t next_lane = next_fast_lane;
			if (st.next_desired_lane!=-1) next_lane = st.next_desired_lane;
			if (st.last_switch==0) {
				auto&np = get_piece(next_piece_index);
				//if (np.is_switch && !np.angle && st.piece_pos+st.speed*3>=get_piece_length(st.piece_index,st.start_lane,st.end_lane)) {
				if (np.is_switch && st.piece_pos+st.speed*3>=get_piece_length(st.piece_index,st.start_lane,st.end_lane)) {
				//if (np.is_switch) {
					if (st.end_lane!=next_lane) {
						a.t = action::t_switch;
						if (next_lane>st.end_lane) a.v = 1;
						else a.v = -1;
					}
				} else st.next_desired_lane = -1;
			}
			if (st.has_turbo && a.t==action::t_throttle && !st.turbo_ticks) {
				//a.t = action::t_turbo;
			}
			if (eval_force_turbo) {
				a.t = action::t_turbo;
				eval_force_turbo = false;
			}
			auto adv = [&](simul::state&st,action&a) {
				//st.bumps.reset();
				//if (st.tick && st.tick%600==0 && !st.crashed_ticks) st.has_turbo = true;
				auto do_bumps = [&](simul::state&st) {
					if (cur_idx==my_car_index && st.bumps2.any()) {
						if (std::abs(st.angle)>=crash_angle*0.75) st.crashed_ticks = 400;
					}
					if (cur_idx==my_car_index && (st.bumps.any() || st.survived_bumps.any())) {
						if (std::abs(st.angle)>=crash_angle*0.75) st.crashed_ticks = 400;
					}
					if (!st.crashed_ticks) {
						double my_pos = get_abs_piece_pos(st.piece_index,st.start_lane) + st.piece_pos;
						for (size_t idx=0;idx<states.size();++idx) {
							if (idx==cur_idx) continue;
							if (st.bumps2.test(idx)) continue;
							auto&n_states = states[idx];
							size_t i = st.tick;
							if (n_states.size()>i) {
								auto&n_st = n_states[i];
								if (n_st.tick!=st.tick) xcept("tick mismatch a - %d vs %d",n_st.tick,st.tick);
								if (n_st.crashed_ticks) continue;
								if (st.start_lane!=n_st.start_lane || st.end_lane!=n_st.end_lane) continue;
								double n_pos = get_abs_piece_pos(n_st.piece_index,n_st.start_lane) + n_st.piece_pos;
								double d = n_pos - my_pos;

								if (std::abs(d)<race.cars[0].length) {
									st.bumps2.set(idx);
									if (d>0) {
										log("bump (into %d) at tick %d - %d %f / %d %f (%f / %f), d is %f\n",idx,st.tick,st.piece_index,st.piece_pos,n_st.piece_index,n_st.piece_pos,my_pos,n_pos,d);
										if (std::abs(st.angle)>=crash_angle/6) {
											//if (cur_idx==my_car_index && my_bump_is_crash) st.crashed_ticks = 400;
										}
										//st.speed = n_st.speed * 0.8;
									}
								}
							}
						}
						for (size_t idx=0;idx<states.size();++idx) {
							if (idx==cur_idx) continue;
// 							if (st.bumps.test(idx)) continue;
// 							if (st.survived_bumps.test(idx)) continue;
							if (race_positions[idx]>race_positions[cur_idx]) continue;
							auto&all_n_states = evil_throttle_states[idx];
							for (size_t idx2=0;idx2<all_n_states.size();++idx2) {
								if (st.bumps.test(idx*32 + idx2)) continue;
								if (st.survived_bumps.test(idx*32 + idx2)) continue;
								auto&n_states = all_n_states[idx2];
								size_t i = st.tick;
								if (n_states.size()>i) {
									auto&n_st = n_states[i];
									if (n_st.tick!=st.tick) xcept("tick mismatch b - %d vs %d",n_st.tick,st.tick);
									if (n_st.crashed_ticks) continue;
									if (st.start_lane!=n_st.start_lane || st.end_lane!=n_st.end_lane) continue;
									double n_pos = get_abs_piece_pos(n_st.piece_index,n_st.start_lane) + n_st.piece_pos;
									double d = n_pos - my_pos;

									if (std::abs(d)<race.cars[0].length) {
										if (d<0) {
											//st.bumps.set(idx);
											st.bumps.set(idx*32 + idx2);
											//st.bump_idx = idx2;
											log("bump (rammed by %d) at tick %d - %d %f / %d %f (%f / %f), d is %f\n",idx,st.tick,st.piece_index,st.piece_pos,n_st.piece_index,n_st.piece_pos,my_pos,n_pos,d);
											st.speed = n_st.speed * 0.9;
											//st.piece_pos = n_st.piece_pos + race.cars[0].length;
											st.piece_pos += (n_pos+race.cars[0].length) - my_pos;
										} else {
	// 										st.survived_bumps.set(idx);
	// 										log("bump (into full throttle %d) at tick %d - %d %f / %d %f (%f / %f), d is %f\n",idx,st.tick,st.piece_index,st.piece_pos,n_st.piece_index,n_st.piece_pos,my_pos,n_pos,d);
	// 										st.speed = n_st.speed * 0.8;
										}
									}
								}
							}
						}
					}
				};

				size_t pidx = st.piece_index;
				sim.advance(st,a,do_bumps);
				++advance_count;
				if (pidx!=st.piece_index && st.piece_index==initial_states[cur_idx].piece_index) {
					++st.laps;
				}
				if (pidx!=st.piece_index && st.piece_index==0) {
					++st.laps_finished;
					++st.lap;
				}


			};
			adv(st,a);
// 
// 			if (last_st.piece_index!=st.piece_index) {
// 				//if (st.piece_abs_distance>=1000.0) break;
// 				//if (st.piece_index==initial_states[cur_idx].piece_index) break;
// 				if (st.piece_index==initial_states[cur_idx].piece_index) {
// 					++laps;
// 					if (laps>=3) break;
// 				}
// 			}

			double max_angle = crash_angle;

			auto redo = [&]() {

				auto eval = [&](size_t notouch,size_t low,size_t high,size_t max,double boundary_throttle) {
					size_t b = cur_states.size() - max;
					size_t e = cur_states.size();
					size_t h = cur_states.size() - high;
					size_t l = cur_states.size() - low;
					size_t n = cur_states.size() - notouch;
					if (boundary_throttle<0) boundary_throttle = 0.0;
					if (boundary_throttle>1.0) boundary_throttle = 1.0;
					//boundary_throttle = 0.5;
					log("redo from %d to %d - low %d high %d boundary %f\n",b,e,low,high,boundary_throttle);
					for (size_t i=b;i!=e;++i) {
						auto&a = cur_actions[i];
						if (a.t==action::t_throttle && i<n) {
							a.v = i>=l || i<h ? default_throttle : 0.0;
							//if (i==l) a.v = boundary_throttle;
							if (i==h) a.v = boundary_throttle;
						}
						auto&prev_st = cur_states[i-1];
						auto&st = cur_states[i];

						st = prev_st;
						adv(st,a);
						if (st.crashed_ticks && !prev_st.crashed_ticks) {
							return true;
						}
					}
					return false;
				};

				size_t low = 0;
				size_t high = std::min<size_t>(pad_n+1,cur_states.size()-1);

// 				for (size_t i=pad_n + 4;i<=32;i+=4) {
// 					if (cur_actions.size()<=i) break;
// 					//auto a = cur_actions[cur_actions.size()-1-i];
// 					auto tmp = cur_actions;
// 					if (!eval(i-1,i-1,i,i,0)) {
// 						//low = i-1;
// 						//high = i;
// 						log("woot %d!\n",i);
// 						break;
// 					} else {
// 						//cur_actions[cur_actions.size()-i-1] = a;
// 						cur_actions = tmp;
// 						eval(i,i,i,i,0);
// 					}
// 				}

				while (low<high) {
					bool crash = eval(0,low,high,high,0);
					if (crash) {
						if (high==cur_states.size()-1) {
							log("redo failed, high %d\n",high);
							return false;
						}
						//high = high + high/2 + 1;
						//high = high + high/2 + 1;
						high = high + high/3 + 7;
						if (high>cur_states.size()-1) high = cur_states.size()-1;
					} else {

						//size_t min_high = low<cur_states.size()-1 ? low + 1 : 0;
// 						size_t min_high = high - high/2 - 1;
// 						while (high>=min_high) {
// 							size_t mid = high - (high-min_high)/2;
// 							if (eval(0,low,mid,high,0)) {
// 								min_high = mid+1;
// 							} else {
// 								high = mid-1;
// 							}
// 						}
// 						if (eval(0,low,high,high,0)) {
// 							if (high<cur_states.size()-1) ++high;
// 							if (eval(0,low,high,high,0)) xcept("waa failed");
// 						}

						size_t max_low = high>0 ? high : 0;
						while (low<=max_low) {
							size_t mid = low + (max_low-low)/2;
							if (eval(0,mid,high,high,0)) {
								max_low = mid-1;
							} else {
								low = mid+1;
							}
						}
						if (low>0) --low;
						if (eval(0,low,high,high,0)) xcept("waa failed");

						double min = 0.0;
// 						double max = 1.0;
// 						while (min<max) {
// 							double mid = min + (max-min)/2;
// 							if (eval(0,low,high,high,mid)) {
// 								max = mid - 1.0/32;
// 							} else {
// 								min = mid + 1.0/32;
// 							}
// 						}
// 						//min = min + (max-min)/2;
// 						if (eval(0,low,high,high,min)) {
// 							min -= 1.0/32;
// 							if (eval(0,low,high,high,min)) xcept("waa failed");
// 						}
						log("redo success, low %d high %d boundary %f\n",low,high,min);
						return true;
					}
				}
				return false;
			};

			if (st.crashed_ticks && !last_st.crashed_ticks) {
				log("crashed at %d\n",st.tick);

				for (size_t i=0;i<pad_n;++i) {
					cur_actions.push_back(action(action::t_throttle,1.0));
					cur_states.push_back(cur_states.back());
					auto&a = cur_actions.back();
					auto&st = cur_states.back();

					adv(st,a);
				}

				redo();
			}
		}

		if (old_size>60) {
			auto&new_st = cur_states.back();
			auto&old_st = cur_states[old_size-60];
			auto prev_bumps = old_st.bumps;
			bool stop = false;
			for (size_t i=old_size-59;i<old_size;++i) {
				auto&st = cur_states[i];
				auto bumps = st.bumps;
				auto c = bumps&~prev_bumps;
				if (c.any()) {
					log("there is a bump at tick %d\n",st.tick);
					for (size_t idx_n : c) {
						size_t idx = idx_n/32;
						size_t idx2 = idx_n%32;
						{
							for (size_t i=0;i<cur_states.size();++i) {
								auto&st = cur_states[i];
								if (st.bumps.test(idx)) {
									log("first bump with %d is at tick %d\n",idx,st.tick);
									break;
								}
							}
						}
						if (new_st.crashed_ticks) {
							log("could not survive bump with %d at tick %d\n",idx,st.tick);

							if (race_positions[cur_idx]>race_positions[idx] || true) {
								bool okay = false;
								//auto&all_n_states = evil_throttle_states[idx];
								auto&all_n_states = evil_throttle_states[idx];
								//size_t idx2 = (size_t)st.bump_idx;
								if (idx2>=all_n_states.size()) log(log_level_info," warning: invalid bump_idx %d\n",idx2);
								if (idx2<all_n_states.size()) {
									auto&n_states = all_n_states[idx2];
									if (n_states.size()>(size_t)st.tick) {
										auto new_st = n_states[st.tick];
										new_st.speed = st.speed * 0.8;
										auto&st = new_st;
										for (int i=0;i<10;++i) {
											sim.advance(st,action(action::t_throttle,0.0));
											++advance_count;
											if (st.crashed_ticks) break;
										}
										okay = !st.crashed_ticks;
									}
								}
								if (!okay && idx2<all_n_states.size()) {
									auto&n_states = all_n_states[idx2];
									if (n_states.size()>(size_t)st.tick) {
										auto new_st = n_states[st.tick];
										new_st.speed = st.speed * 0.8;
										auto&st = new_st;
										for (int i=0;i<4;++i) {
											sim.advance(st,action(action::t_throttle,1.0));
											++advance_count;
											if (st.crashed_ticks) break;
										}
										for (int i=0;i<6;++i) {
											sim.advance(st,action(action::t_throttle,0.0));
											++advance_count;
											if (st.crashed_ticks) break;
										}
										okay = !st.crashed_ticks;
									}
								}
								if (okay && false) {

									size_t i = 1;
									for (;i<(size_t)st.tick;++i) {
										if (i>=cur_states.size()) break;
										auto&st = cur_states[i];
										if (!get_piece(st.piece_index).is_switch) break;
									}
									for (;i<(size_t)st.tick;++i) {
										if (i>=cur_states.size()) break;
										auto&st = cur_states[i];
										if (get_piece(st.piece_index).is_switch) {
											okay = false;
											break;
										}
									}

								}
								if (okay) {
									evil_bumps.emplace_back(idx,cur_idx,st.tick,idx2);
								}
							}
// 							auto&n_actions = actions[idx];
// 							auto&n_states = states[idx];
// 							n_actions.resize(i);
// 							n_states.resize(i);
// 							for (size_t i=0;i<n_states.size();++i) {
// 								n_states[i].bumps2.set(idx);
// 							}
						} else {
							log("survived bump with %d (%d) at tick %d (%d ticks later)\n",idx,idx2,st.tick,new_st.tick-st.tick);
							cur_actions.resize(i);
							cur_states.resize(i);
							for (size_t i=0;i<cur_states.size();++i) {
								cur_states[i].survived_bumps.set(idx_n);
							}
							//cur_states.back().survived_bumps.set(idx);
							stop = true;
							break;
						}

					}
					if (stop) break;
				}
				prev_bumps = bumps;
			}

// 			if (!stop) {
// 				if (cur_idx==my_car_index && my_bump_is_crash && new_st.crashed_ticks && new_st.bumps2.any()) {
// 					my_bump_is_crash = false;
// 					cur_actions.resize(old_size);
// 					cur_states.resize(old_size);
// 					stop = true;
// 				}
// 			}

		}

		
		return false;
	};

	while (true) {

		a_vector<std::tuple<double,size_t>> sorted;
		for (size_t i=0;i<states.size();++i) {
			auto&st = states[i].back();
			double pos = get_abs_center_piece_pos(st.piece_index);
			pos += (st.piece_pos / get_piece_length(st.piece_index,st.start_lane,st.end_lane)) * get_center_piece_length(st.piece_index);
			if (i==my_car_index) pos = 0;
			sorted.emplace_back(-pos,i);
		}
		std::sort(sorted.begin(),sorted.end());

		size_t done_count = 0;
		//for (size_t i=0;i<states.size();++i) {
		for (auto&v : sorted) {
			size_t i = std::get<1>(v);
			//if (eval(i)) ++done_count;
			if (i==my_car_index) {
				auto&cur_actions = actions[i];
				auto&cur_states = states[i];
				auto orig_actions = cur_actions;
				auto orig_states = cur_states;
				auto&initial_st = cur_states.back();
				int desired_lane = cur_states.back().next_desired_lane;
				if (initial_st.last_switch || !get_piece((initial_st.piece_index+1)%race.track.pieces.size()).is_switch) {
					while (!eval(i));
				} else {
					a_vector<action> best_actions;
					a_vector<simul::state> best_states;

					size_t next_fast_lane;
					{
						auto&st = cur_states.back();
						size_t next_piece_index = st.piece_index;
						for (int i=0;i<10;++i) {
							++next_piece_index;
							if (next_piece_index==race.track.pieces.size()) next_piece_index=0;
							if (get_piece(next_piece_index).is_switch) break;
						}
						size_t next_next_piece_index = next_piece_index==race.track.pieces.size()-1 ? 0 : next_piece_index+1;
						next_fast_lane = fast_lane_for_piece[next_next_piece_index];
					}

					double best_score = 0;
					for (int sw=-1;sw<=1;++sw) {
						if (sw!=-1) {
							cur_actions = orig_actions;
							cur_states = orig_states;
						}
						auto&st = cur_states.back();
						if (sw==-1 && st.end_lane==0) continue;
						if (sw==1 && st.end_lane==race.track.lanes.size()-1) continue;
						int this_desired_lane = st.end_lane + sw;
						st.next_desired_lane = this_desired_lane;
						//log(log_level_info,"st.tick is %d, st.next_desired_lane is %d, st.last_switch is %d\n",st.tick,st.next_desired_lane,st.last_switch);
						while (!eval(i)) ;
						
						{
// 							auto&st = cur_states.back();
// 
// 							double s = st.piece_abs_distance;
// 							s += (st.piece_pos / get_piece_length(st.piece_index,st.start_lane,st.end_lane)) * get_center_piece_length(st.piece_index);
// 							s /= cur_states.size();
							auto&st = cur_states[std::min(cur_states.size()-1,(size_t)180)];
							double s = st.piece_abs_distance;
							s += (st.piece_pos / get_piece_length(st.piece_index,st.start_lane,st.end_lane)) * get_center_piece_length(st.piece_index);

// 							for (size_t i=0;i<cur_actions.size();++i) {
// 								auto&a = cur_actions[i];
// 								if (a.t==action::t_switch) {
// 									log(log_level_info," -- switch %d at tick %d\n",(int)a.v,i);
// 								}
// 							}

							for (size_t i=0;i<initial_states.size();++i) {
								if (i==my_car_index) continue;
								auto&ist = initial_states[i];
								if (!ist.crashed_ticks) continue;
								for (int offset=-10;offset<20;offset+=2) {
									size_t idx = ist.crashed_ticks + offset;
									if (cur_states.size()<=idx) continue;
									auto&st = cur_states[idx];
									if (st.start_lane!=ist.start_lane || st.end_lane!=ist.end_lane) continue;
									double my_pos = get_abs_piece_pos(st.piece_index,st.start_lane) + st.piece_pos;
									double n_pos = get_abs_piece_pos(ist.piece_index,ist.start_lane) + ist.piece_pos;
									if (std::abs(my_pos-n_pos)<=40.0) {
										s *= 0.5;
										log(log_level_info,"avoiding passing dangerously close to respawning %d in lane %d\n",i,this_desired_lane);
										break;
									}
								}
							}

							s *= traffic_factors.at(this_desired_lane);
							
							if (this_desired_lane==next_fast_lane) s *= 1.0675;

							log("score for lane %d is %f\n",this_desired_lane,s);

							if (s>best_score) {
								best_score = s;
								best_actions = std::move(cur_actions);
								best_states = std::move(cur_states);
								desired_lane = this_desired_lane;
							}

						}
					}
					cur_actions = std::move(best_actions);
					cur_states = std::move(best_states);
				}

				if (!orig_states.back().turbo_ticks && orig_states.back().has_turbo && !cur_states.back().crashed_ticks) {

					double best_pos = 0;
					int best_n = 0;
					int best_i = 0;

					int next_turbo = turbo_available_in;
					if (next_turbo<=0) next_turbo = 1;
					if (next_turbo>60*20) next_turbo = 60*20;

					{
						auto old_actions = cur_actions;
						auto old_states = cur_states;

						eval_override_ticks = true;
						eval_ticks = next_turbo;
						while (!eval(i));
						eval_override_ticks = false;
						log("next_turbo is %d, cur_states.size() is %d\n",next_turbo,cur_states.size());

						int step = 5;
						for (size_t i=std::min(next_turbo-1,step-1-(current_frame%step));i<std::min(cur_states.size(),(size_t)next_turbo);i+=step) {

							auto&orig_st = cur_states[i];
							simul::state st = orig_st;
							//log(log_level_info,"states[%d] is at %d %f\n",i,st.piece_index,st.piece_pos);

							sim.advance(st,action(action::t_turbo));
							++advance_count;
							int n = 0;
							{
								//size_t max_i = std::min(cur_states.size(),std::min(next_turbo - i,(size_t)60));
								size_t max_i = std::min(cur_states.size(),(size_t)60);
								for (size_t i=0;i<max_i;++i) {
									sim.advance(st,action(action::t_throttle,1.0));
									++advance_count;
									if (st.crashed_ticks) break;
									++n;
								}
							}
							// 						if (!st.crashed_ticks) {
							// 							for (int i=0;i<30;++i) {
							// 								sim.advance(st,action(action::t_throttle,0.0));
							// 								++advance_count;
							// 								if (st.crashed_ticks) break;
							// 								++n;
							// 							}
							// 						}

							if (n>best_n) {
								double pos = 0;
								pos += get_abs_center_piece_pos(orig_st.piece_index);
								pos += (orig_st.piece_pos / get_piece_length(orig_st.piece_index,orig_st.start_lane,orig_st.end_lane)) * get_center_piece_length(orig_st.piece_index);

								best_n = n;
								best_pos = pos;
								best_i = i;
								//log(log_level_info,"best is %d with pos %f\n",i,pos);
							}

							if (i>60*4) i += step;
							//if (i>60*6) i += step;
						}

						cur_actions = std::move(old_actions);
						cur_states = std::move(old_states);

					}

					log("best pos for turbo is %f (tick %d) with %d\n",best_pos,best_i,best_n);

					if (orig_states.back().has_turbo) {
						if (standings[0].second==my_car_index && standings.size()>1) {
							//log(log_level_info,"i am first, don't use turbo\n");
							//best_i = 1;
							size_t idx = standings[1].second;
							double distance = standings[0].first - standings[1].first;
							log(log_level_info,"%d is %f behind me\n",idx,distance);
// 							if (distance<=100 && initial_states[idx].turbo_ticks) {
// 								best_i = 0;
// 							}
						}
					}

					if (best_i==0) {

						bool okay = true;
						

						if (okay) {
							
							auto nonturbo_actions = std::move(cur_actions);
							auto nonturbo_states = std::move(cur_states);
							cur_actions = nonturbo_actions;
							cur_states = nonturbo_states;
							cur_actions.resize(1);
							cur_states.resize(1);

							eval_force_turbo = true;
							while (!eval(i));
							eval_force_turbo = false;

							auto&st = cur_states.back();
							if (st.crashed_ticks) {
								cur_actions = nonturbo_actions;
								cur_states = nonturbo_states;
							}

						}
					}

				}

			} else {
				while (!eval(i));

				auto&cur_actions = actions[i];
				auto&cur_states = states[i];
				for (size_t tick=1;tick<30;tick+=1) {
					if (tick>=cur_states.size()) break;

					a_vector<action> new_actions;
					a_vector<simul::state> new_states;

					new_actions.resize(tick);
					new_states.resize(tick);
					std::copy(cur_actions.begin(),cur_actions.begin()+tick,new_actions.begin());
					std::copy(cur_states.begin(),cur_states.begin()+tick,new_states.begin());

					for (size_t n=2;n<30;n+=2) {
						new_states.push_back(new_states.back());
						auto&st = new_states.back();
						++advance_count;
						action a(action::t_throttle,1.0);
						if (st.has_turbo && !st.turbo_ticks) a.t = action::t_turbo;
						new_actions.push_back(a);
						sim.advance(st,a);
						if (st.crashed_ticks) break;
					}

					evil_throttle_actions[i].push_back(std::move(new_actions));
					evil_throttle_states[i].push_back(std::move(new_states));
				}
			}
			{
				auto&cur_actions = actions[i];
				auto&cur_states = states[i];
				if (cur_states.back().crashed_ticks && !initial_states[i].crashed_ticks) {

					bool okay = false;
					for (size_t n=0;n<4;++n) {
						a_vector<action> emergency_actions;
						a_vector<simul::state> emergency_states;

						emergency_actions.push_back(action());
						emergency_states.push_back(initial_states[i]);
						emergency_states.reserve(10);
						for (size_t t=0;t<30;++t) {
							emergency_states.push_back(emergency_states.back());
							auto&st = emergency_states.back();
							++advance_count;
							action a(action::t_throttle,0.0);
							if (t<n) a.v = 1.0;
							emergency_actions.push_back(a);
							sim.advance(st,a);
							if (st.crashed_ticks) break;
						}
						if (!emergency_states.back().crashed_ticks) {
							log(log_level_info,"emergency actions %d succeeded for %d\n",n,i);

							actions[i] = std::move(emergency_actions);
							states[i] = std::move(emergency_states);

							okay = true;
							break;
						}
					}
					if (!okay) {
						log(log_level_info,"emergency actions failed for %d\n",i);
					}
				}
			}

			++done_count;
		}
		if (done_count==states.size()) break;
	}

	for (auto&v : evil_bumps) {
		size_t back, front, tick, idx2;
		std::tie(back,front,tick,idx2) = v;
		log(log_level_info,"found evil bump for car %d against %d at tick %d (idx2 %d)\n",back,front,tick,idx2);
		if (back==my_car_index) {
			auto&car_pos = st.cars[my_car_index];
			//if (car_pos.has_turbo || car_pos.turbo_ticks) {
				log(log_level_info,"executing evil bump! muahaha\n");
				auto&cur_actions = actions[my_car_index];
				auto&cur_states = states[my_car_index];
				cur_states = evil_throttle_states[my_car_index][idx2];
				cur_actions = evil_throttle_actions[my_car_index][idx2];

				cur_states.resize(tick);
				cur_actions.resize(tick);
			//}
// 			cur_actions.clear();
// 			for (size_t i=0;i<cur_states.size();++i) {
// 				action a(action::t_throttle,1.0);
// 				if (i==0 && st.has_turbo) a.t = action::t_turbo;
// 				cur_actions.push_back(a);
// 			}
		}
	}
	if (current_log_level<=log_level_debug) {
		for (size_t i=0;i<states.size();++i) {

			auto&cur_actions = actions[i];
			auto&cur_states = states[i];

			log("car %d%s - \n",i,i==my_car_index ? " (me)" : "");
			log("  %d states\n",cur_states.size());
			for (size_t i=0;i<cur_states.size();++i) {
				auto&a = cur_actions[i];
				auto&st = cur_states[i];
				const char*str = "?";
				if (a.t==action::t_throttle) str = format("throttle %g",a.v);
				else if (a.t==action::t_switch) str = format("switch %g",a.v);
				else if (a.t==action::t_turbo) str = "turbo";

				log("  %4d- %s   - at %d %f - speed %f  angle %f\n",st.tick,str,st.piece_index,st.piece_pos,st.speed,st.angle*180/PI);
			}

		}
	}

	log("advance count is %d\n",advance_count);

	find_actions = std::move(actions);
	find_states = std::move(states);
	find_advance_count = advance_count;

}

a_vector<action> my_actions;
a_vector<simul::state> my_states;
size_t my_index = 0;

template<typename T>
double get_car_pos(T&st) {
	double track_length = get_abs_center_piece_pos(race.track.pieces.size()-1) + get_center_piece_length(race.track.pieces.size()-1);
	double pos = track_length * st.lap;
	pos += get_abs_center_piece_pos(st.piece_index);
	pos += (st.piece_pos / get_piece_length(st.piece_index,st.start_lane,st.end_lane)) * get_center_piece_length(st.piece_index);
	return pos;
}

a_deque<std::tuple<size_t,int,double,double>> predicted_car_pos;

action test() {

	tsc::high_resolution_timer ht;

	sim = simul();
	sim.turbo_factor = turbo_factor ? turbo_factor : 3.0;
	sim.turbo_ticks = turbo_ticks ? turbo_ticks : 30;

	{
		static int last_calc = -100;
		auto&car_pos = st.cars[my_car_index];
		if (current_frame==0) {
			last_calc = current_frame;
			calculate_fast_lanes();


			default_throttle_for_car.resize(st.cars.size(),1.0);
		}
	}

	while (!predicted_car_pos.empty() && std::get<1>(predicted_car_pos.front())<=current_frame) {
		auto v = predicted_car_pos.front();
		predicted_car_pos.pop_front();
		size_t car_index;
		int tick;
		double source_pos, predicted_pos;
		std::tie(car_index,tick,source_pos,predicted_pos) = v;
		
		double actual_pos = get_car_pos(st.cars.at(car_index));
		
		double predicted_distance = predicted_pos-source_pos;
		double actual_distance = actual_pos-source_pos;

		log(log_level_info,"predicted distance for %d is %f, actual distance %f\n",car_index,predicted_distance,actual_distance);

		if (car_index!=my_car_index && !st.cars.at(car_index).crashed_ticks) {

			double v = actual_distance/predicted_distance;

			if (default_throttle_for_car.size()<=car_index) default_throttle_for_car.resize(car_index+1);
			double&t = default_throttle_for_car[car_index];
			t *= v;
			if (t<0) t = 0.25;
			if (t>1) t = 1;
			if (!(t>0) && !(t<1)) t = 0.25;

			log(log_level_info,"%d moved %f, default throttle %f\n",car_index,v,t);

		}

	}

	if (my_index>=my_states.size() || true) {
	//if (my_index>=my_states.size()) {
		//if (!my_states.empty()) xcept("stop");
		//find_max_ticks = 60*60;
		find();
		auto&my_new_states = find_states[my_car_index];
		if (my_new_states.back().crashed_ticks && my_index<my_states.size()/2) {
			log(log_level_info," not using new path since it will crash\n");
		} else {
			my_actions = find_actions[my_car_index];
			my_states = find_states[my_car_index];
			my_index = 0;
		}

		size_t ticks = my_states.size() - 2;
		log(" time: %d ticks - %.02fs\n",ticks,ticks/60.0);
		//if (ticks>=50*60) xcept("stop");
		//xcept("stop");

		if (current_frame%15==0) {
			for (size_t i=0;i<find_states.size();++i) {
				auto&cur_states = find_states[i];
				if (cur_states.size()<=30) continue;
				predicted_car_pos.emplace_back(i,current_frame+30,get_car_pos(st.cars.at(i)),get_car_pos(cur_states[30]));
			}
		}
	}
	action a;
	if (my_index<my_states.size()) {

		auto&car_pos = st.cars[my_car_index];
		auto&my_st = my_states[my_index];
// 		if (car_pos.crashed_ticks) {
// 			fopen("crashed.txt","w");
// 			xcept("crashed");
// 		}
		if (all_states.size()>8) {
			if (std::abs(my_st.piece_pos-car_pos.piece_pos)>=1e-5) {
				log(log_level_info,"piece_pos mismatch - %.100g vs %.100g\n",my_st.piece_pos,car_pos.piece_pos);
				//xcept("mismatch");
				my_states.clear();
			}
			if (std::abs(my_st.speed-car_pos.speed)>=1e-5) {
				log(log_level_info,"speed mismatch - %.100g vs %.100g\n",my_st.speed,car_pos.speed);
				//xcept("mismatch");
				my_states.clear();
			}
			if (std::abs(my_st.angle-car_pos.angle)>=1e-8) {
				log(log_level_info,"angle mismatch - %.100g vs %.100g\n",my_st.angle*180/PI,car_pos.angle*180/PI);
				//xcept("mismatch");
				my_states.clear();
			}
		}
		++my_index;
		if (my_index<my_states.size()) a = my_actions[my_index];
// 		if (a.t!=action::t_turbo) {
// 			my_actions.clear();
// 			my_states.clear();
// 		}
	}

	log(log_level_info,"opt took %fms    advance_count is %d\n",ht.elapsed()*1000.0,find_advance_count);

	return a;
}


}