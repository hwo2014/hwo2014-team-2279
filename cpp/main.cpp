
#define _VARIADIC_MAX 8

#define _CRT_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS

#include <cstdlib>
#include <cstdio>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#include <Windows.h>
#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib,"winmm.lib")
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#endif

#undef min
#undef max
#undef near
#undef far


#include <cmath>
#include <cstddef>
#include <cstring>

#include <array>
#include <vector>
#include <set>
#include <map>
#include <list>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <functional>
#include <numeric>
#include <memory>
#include <random>
#include <chrono>
#include <thread>
#include <initializer_list>
#include <mutex>
#include <algorithm>
#include <utility>
#include <typeinfo>
using std::type_info;

template<int=0,typename...args>
void xcept(args&&...v);

// template<typename T>
// struct alloc : std::allocator<T> {
// 	alloc() {}
// 	alloc(const alloc&other) : alloc() {}
// 	template<typename t>
// 	alloc(const alloc<t>&other) : alloc() {}
// 	template<typename t>
// 	struct rebind {
// 		typedef alloc<t> other;
// 	};
// };

#include "tsc/alloc.h"
using tsc::alloc;
#include "tsc/alloc_containers.h"

#include "tsc/strf.h"

int current_frame;

struct simple_logger {
	std::mutex mut;
	tsc::a_string str, str2;
	bool newline;
	FILE*f;
	const char*filename;
	simple_logger() : newline(true), f(0), filename(nullptr) {
#ifdef LOCAL
		filename = "log.txt";
#endif
	}
	template<typename...T>
	void operator()(const char*fmt,T&&...args) {
		std::lock_guard<std::mutex> lock(mut);
		if (!f && filename && *filename) f = fopen(filename,"w");
		tsc::strf::format(str,fmt,std::forward<T>(args)...);
		if (newline) tsc::strf::format(str2,"%5d: %s",current_frame,str);
		const char*out_str= newline ? str2.c_str() : str.c_str();
		newline = strchr(out_str,'\n') ? true : false;
		if (f) {
			fputs(out_str,f);
			fflush(f);
		}
		fputs(out_str,stdout);
	}
};
simple_logger logger;

enum {
	log_level_all,
	log_level_debug,
	log_level_info
};

int current_log_level = log_level_info;

template<typename...T>
void log(int level,const char*fmt,T&&...args) {
	if (current_log_level<=level) logger(fmt,std::forward<T>(args)...);
}

template<typename...T>
void log(const char*fmt,T&&...args) {
	log(log_level_debug,fmt,std::forward<T>(args)...);
}


struct xcept_t {
	tsc::a_string str1, str2;
	int n;
	xcept_t() {
		str1.reserve(0x100);
		str2.reserve(0x100);
		n = 0;
	}
	template<typename...T>
	void operator()(const char*fmt,T&&...args) {
		try {
			auto&str = ++n%2 ? str1 : str2;
			tsc::strf::format(str,fmt,std::forward<T>(args)...);
			log(log_level_info,"about to throw exception %s\n",str);
			throw (const char*)str.c_str();
		} catch (const std::bad_alloc&) {
			throw (const char*)fmt;
		}
	}
};
xcept_t xcept_wrapper;
template<int,typename...args>
void xcept(args&&...v) {
	xcept_wrapper(std::forward<args>(v)...);
}

tsc::a_string format_string;
template<typename...T>
const char*format(const char*fmt,T&&...args) {
	return tsc::strf::format(format_string,fmt,std::forward<T>(args)...);
}

//#include <tsc/coroutine.h>
//#include "tsc/userthreads.h"

#include "tsc/high_resolution_timer.h"
#include "tsc/rng.h"
#include "tsc/bitset.h"

using tsc::rng;
using tsc::a_string;
using tsc::a_vector;
using tsc::a_deque;
using tsc::a_list;
using tsc::a_set;
using tsc::a_multiset;
using tsc::a_map;
using tsc::a_multimap;
using tsc::a_unordered_set;
using tsc::a_unordered_multiset;
using tsc::a_unordered_map;
using tsc::a_unordered_multimap;
//using tsc::a_circular_queue;


const double PI = 3.1415926535897932384626433;


#include "json.h"
#include "bot.h"

#ifndef _WIN32
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#endif

struct conn {
	int fd;
	conn() {
		fd = socket(AF_INET,SOCK_STREAM,0);
		if (fd==-1) xcept("socket failed; error %d",errno);
	}
	~conn() {
#ifdef _WIN32
		if (fd!=-1) closesocket(fd);
#else
		if (fd!=-1) close(fd);
#endif
	}
	void connect(a_string hostname,int port) {
		addrinfo*ai;
		if (getaddrinfo(hostname.c_str(),format("%d",port),0,&ai)) xcept("getaddrinfo failed");
		if (::connect(fd,ai->ai_addr,ai->ai_addrlen)) xcept("connect failed");
		log(log_level_info,"connected\n");

		int opt = 1;
		if (setsockopt(fd,IPPROTO_TCP,TCP_NODELAY,(char*)&opt,sizeof(opt))) log(log_level_info,"setsockopt(TCP_NODELAY) failed");
	}
	void send(a_string data) {
		//log(log_level_info,"send %d bytes: %s\n",data.size(),data);
		int r = ::send(fd,(const char*)data.data(),data.size(),0);
		if (r!=data.size()) xcept("send failed");
	}
	a_string recv() {
		char buf[0x1000];
		int r = ::recv(fd,buf,0x1000,0);
		if (r==0) xcept("connection closed");
		if (r<0) xcept("recv failed");
		//log(log_level_info,"received %d bytes: %.*s\n",r,r,buf);
		return {buf,(size_t)r};
	}
};





int main(int argc, const char**argv) {
	try {

		a_string server, name, key;
		int port = 0;
		bool join_race = false, create_race = false;
		int car_count = 1;
		a_string track, password;

		auto join_create_params = [&](const char*a) {
			const char*c = a;
			while (*c && *c!=',') ++c;
			car_count = atoi(a);
			if (*c) ++c;
			a = c;
			while (*c && *c!=',') ++c;
			track = {a,(size_t)(c-a)};
			if (*c) ++c;
			a = c;
			while (*c) ++c;
			password = {a,(size_t)(c-a)};
		};
		int argn = 0;
		for (int i=1;i<argc;++i) {
			const char*a = argv[i];
			bool is_j = 0==strcmp(a,"-j");
			bool is_c = 0==strcmp(a,"-c");
			if (is_j || is_c) {
				++i;
				if (i==argc) xcept("%s requires an argument",a);
				join_race = is_j;
				create_race = is_c;
				join_create_params(argv[i]);
				continue;
			}
			if (!strcmp(a,"-l")) {
				++i;
				if (i==argc) xcept("%s requires an argument",a);
				if (logger.f) xcept("-l: log file already opened");
				logger.filename = argv[i];
				continue;
			}
			if (!strcmp(a,"-v")) {
				current_log_level = log_level_all;
				continue;
			}
			if (argn==0) server = a;
			else if (argn==1) port = atoi(a);
			else if (argn==2) name = a;
			else if (argn==3) key = a;
			else xcept("invalid parameter: %s",a);
			++argn;
		}

		if (server.empty()) {
			server = "testserver.helloworldopen.com";
			port = 8091;
			name = "Moo";
			key = "/GPjkP38D2LMsw";
		}

		if (server.empty() || port==0 || name.empty() || key.empty()) {
			log(log_level_info,"Usage: %s host port botname botkey [-j | -c] [car_count[,track[,password]]] [-l logfile] [-v]\n",argv[0]);
			return 1;
		}


		log(log_level_info,"hello world\n");

#ifdef _WIN32
		WSADATA wsadata;
		WSAStartup(0x0101,&wsadata);
		timeBeginPeriod(1);
#endif

		log(log_level_info,"server '%s' port %d name '%s' key '%s'\n",server,port,name,key);
		if (join_race) log(log_level_info,"joining race, car count %d, track '%s', password '%s'\n",car_count,track,password);
		if (create_race) log(log_level_info,"creating race, car count %d, track '%s', password '%s'\n",car_count,track,password);

		conn c;
		bot::game<> g;
		c.connect(server,port);
		if (join_race) c.send((g.join_race(name,key,car_count,track,password).dump()+"\n").c_str());
		else if (create_race) c.send((g.create_race(name,key,car_count,track,password).dump()+"\n").c_str());
		else c.send((g.join(name,key).dump()+"\n").c_str());
		a_string str;
		tsc::high_resolution_timer ht;
		while (true) {
			json_value jv;
			while (true) {
				char*np = strchr((char*)str.c_str(),'\n');
				if (np) {
					jv.parse(str.c_str(),np-str.c_str());
					str.erase(str.begin(),str.begin()+(np-str.c_str())+1);
					break;
				}
				str += c.recv();
			}
			ht.reset();
			jv = g.message(jv);
			if (ht.elapsed()>1.0/80) log(log_level_info," !! took %fms to process message\n",ht.elapsed()*1000);
			//if (ht.elapsed()>1.0/80) xcept("took %fms to process message\n",ht.elapsed()*1000);
			if (jv.type!=json_value::t_null) c.send((jv.dump()+"\n").c_str());
		}
	} catch (const char*str) {
		log(log_level_info," [exception] %s\n",str);
	} catch (const std::exception&e) {
		log(log_level_info," [exception] %s\n",e.what());
	}
	
	return 0;

}


