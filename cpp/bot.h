

namespace bot {
;

bool playback = false;

struct state {

	int tick;
	struct car_position {
		double angle;
		int piece_index;
		double piece_pos;
		size_t start_lane, end_lane;
		int lap;
		int crashed_ticks;
		bool bumped;
		bool front_bumped;

		double rot, speed, last_throttle;
		bool has_turbo;
		int turbo_ticks;
		bool turbo_next_tick;
		int laps_finished;
	};
	a_vector<car_position> cars;

};

struct car {
	a_string color, name;
	double guide_flag_pos;
	double length, width;
};

struct track_t {
	a_string id, name;
	a_vector<double> lanes;
	struct piece {
		double length;
		double angle, radius;
		bool is_switch;
	};
	a_vector<piece> pieces;
};

struct race {
	a_vector<car> cars;
	track_t track;
	double laps;
	double max_lap_time_ms;
	bool quick_race;
};

track_t::piece&get_piece(size_t index);

struct action {
	enum { t_nothing, t_throttle, t_switch, t_turbo };
	int t;
	double v;
	action() : t(t_nothing) {}
	action(int t) : t(t) {}
	action(int t,double v) : t(t), v(v) {}
};

template<int=0>
struct game;
namespace play{
	action play(game<>&g);
}

int game_start_count;

void calculate_track_values(game<>&g);

template<int>
struct game {
	a_map<a_string,size_t> car_color_map;
	a_unordered_map<size_t,int> car_last_crashed;
	a_unordered_map<size_t,bool> car_dnf;
	a_unordered_map<size_t,int> car_last_turbo;
	a_unordered_map<size_t,bool> car_has_turbo;
	a_unordered_map<size_t,int> car_laps_finished;
	a_vector<state> all_states;
	state st;
	race r;
	size_t my_car_index;
	double last_throttle;
	int last_switch;
	int last_turbo_available, next_turbo_available;
	double turbo_factor;
	int turbo_ticks;
	int respawn_ticks;
	game() : my_car_index(~0), last_throttle(0), last_switch(0), last_turbo_available(0), next_turbo_available(0), turbo_factor(0), turbo_ticks(0), respawn_ticks(400) {}
	json_value join(a_string name,a_string key) {
		json_value jv;
 		jv["msgType"] = "join";
 		jv["data"]["name"] = name;
 		jv["data"]["key"] = key;
		return jv;
	}
	json_value join_race(a_string name,a_string key,int car_count,a_string track_name="",a_string password="") {
		json_value jv;
		jv["msgType"] = "joinRace";
		jv["data"]["botId"]["name"] = name;
		jv["data"]["botId"]["key"] = key;
		jv["data"]["carCount"] = car_count;
		if (!track_name.empty()) jv["data"]["trackName"] = track_name;
		if (!password.empty()) jv["data"]["password"] = password;
		return jv;
	}
	json_value create_race(a_string name,a_string key,int car_count,a_string track_name="",a_string password="") {
		json_value jv;
		jv["msgType"] = "createRace";
		jv["data"]["botId"]["name"] = name;
		jv["data"]["botId"]["key"] = key;
		jv["data"]["carCount"] = car_count;
		if (!track_name.empty()) jv["data"]["trackName"] = track_name;
		if (!password.empty()) jv["data"]["password"] = password;
		return jv;
	}

	size_t get_car_index(const a_string&color) {
		size_t idx = car_color_map.emplace(std::make_pair(color,car_color_map.size())).first->second;
		return idx;
	}
	double get_throttle(double desired_speed);
	template<int=0>
	json_value message(json_value&msg) {
		//log("got message %s\n",msg.dump());
		tsc::high_resolution_timer ht;
		json_value rv;

#ifdef LOCAL
		static FILE*dump_f = fopen("out.txt","w");
		fprintf(dump_f,"%s\n",msg.dump().c_str());
#endif

		auto go = [&]() {
			if (!all_states.empty()) {
				auto&car_pos = st.cars.at(my_car_index);
				auto&last_car_pos = all_states.back().cars.at(my_car_index);
				if (car_pos.piece_index!=last_car_pos.piece_index && get_piece(car_pos.piece_index).is_switch) last_switch = 0;
			}
			all_states.push_back(st);
			action a = play::play(*this);
			//action a(action::t_throttle,0.5);
			//get_throttle(0);

			if (!playback) {
				if (a.t==action::t_nothing) {
					log("action: nothing\n");
					rv["msgType"] = "ping";
					rv["gameTick"] = current_frame;
				} else if (a.t==action::t_throttle) {
					log("action: throttle %f\n",a.v);
					rv["msgType"] = "throttle";
					rv["gameTick"] = current_frame;
					rv["data"] = a.v;
					last_throttle = a.v;
				} else if (a.t==action::t_switch) {
					log("action: switch %f\n",a.v);
					rv["msgType"] = "switchLane";
					rv["gameTick"] = current_frame;
					rv["data"] = a.v>0 ? "Right" : "Left";
					last_switch = (int)a.v; 
				} else if (a.t==action::t_turbo) {
					log("action: turbo\n");
					rv["msgType"] = "turbo";
					rv["gameTick"] = current_frame;
					rv["data"] = "Moooooooooooooooooooooooooooooo";
				}
				else xcept("unknown action");
			}
		};

		a_string t = msg["msgType"];
		if (t=="gameStart") {
			++game_start_count;
			if (current_frame) {
				last_throttle = 0;
				last_switch = 0;
				car_last_crashed.clear();
				car_last_turbo.clear();
				car_has_turbo.clear();
			}
			current_frame = msg["gameTick"];
			st.tick = current_frame;
			go();
		} else if (t=="gameInit") {
			r = race_from_json(*this,msg["data"]["race"]);
			calculate_track_values(*this);
		} else if (t=="yourCar") {
			my_car_index = get_car_index(msg["data"]["color"]);
		} else if (t=="carPositions") {
			st = state_from_json(*this,msg);
			if (msg["gameTick"]) {
				current_frame = st.tick;
				go();
			}
		} else if (t=="crash") {
			size_t idx = get_car_index(msg["data"]["color"]);
			car_last_crashed[idx] = current_frame;
			log(log_level_info,"%d crashed\n",idx);
		} else if (t=="spawn") {
			size_t idx = get_car_index(msg["data"]["color"]);
			if (current_frame-car_last_crashed[idx]!=respawn_ticks) {
				int actual_respawn_ticks = current_frame-car_last_crashed[idx];
				log(log_level_info," warning: respawn time seem to be %d, not %d\n",actual_respawn_ticks,respawn_ticks);
				car_last_crashed[idx] = -actual_respawn_ticks;
				respawn_ticks = actual_respawn_ticks;
			}
			car_has_turbo[idx] = false;
			log(log_level_info,"%d spawned\n",idx);
		} else if (t=="throttle") {
			last_throttle = msg["data"];
		} else if (t=="turboAvailable") {
			log(log_level_info,"turbo available\n");
			if (next_turbo_available && next_turbo_available!=current_frame) log(log_level_info,"next_turbo_available was %d off\n",next_turbo_available-current_frame);
			next_turbo_available = current_frame + (current_frame-last_turbo_available);
			last_turbo_available = current_frame;
			turbo_factor = msg["data"]["turboFactor"];
			turbo_ticks = (int)msg["data"]["turboDurationTicks"] + 1;
			for (size_t i=0;i<r.cars.size();++i) {
				car_has_turbo[i] = true;
			}
		} else if (t=="turboStart") {
			size_t idx = get_car_index(msg["data"]["color"]);
			car_last_turbo[idx] = current_frame;
			car_has_turbo[idx] = false;
			log(log_level_info,"turbo start for %d\n",idx);
		} else if (t=="turboEnd") {
			size_t idx = get_car_index(msg["data"]["color"]);
			log(log_level_info,"turbo end for %d\n",idx);
			if (current_frame-car_last_turbo[idx]!=turbo_ticks) {
				log(log_level_info," warning: turbo for %d lasted for %d ticks, turbo_ticks is %d\n",idx,current_frame-car_last_turbo[idx],turbo_ticks);
				car_last_turbo[idx] = -turbo_ticks;
			}
		} else if (t=="switchLane") {
			last_switch = msg["data"]=="Right" ? 1 : -1;
		} else if (t=="dnf") {
			car_dnf[get_car_index(msg["data"]["car"]["color"])] = true;
		} else if (t=="lapFinished") {
			car_laps_finished[get_car_index(msg["data"]["car"]["color"])] = msg["data"]["raceTime"]["laps"];
		}
			
#ifdef LOCAL
		fprintf(dump_f,"%s\n",rv.dump().c_str());
		fflush(dump_f);
#endif
		
		log(log_level_info,"bot::message took %fms\n",ht.elapsed()*1000);

		return rv;
	}
};

state state_from_json(game<>&g,json_value&jv) {
	state r;
	r.tick = jv["gameTick"];
	r.cars.resize(g.car_color_map.size());
	for (auto&v : jv["data"].vector) {
		size_t idx = g.get_car_index(v["id"]["color"]);
		if (r.cars.size()<=idx) r.cars.resize(idx+1);
		auto&ci = r.cars[idx];
		ci.angle = (double)v["angle"]/180*PI;
		ci.piece_index = v["piecePosition"]["pieceIndex"];
		ci.piece_pos = v["piecePosition"]["inPieceDistance"];
		ci.start_lane = (int)v["piecePosition"]["lane"]["startLaneIndex"];
		ci.end_lane = (int)v["piecePosition"]["lane"]["endLaneIndex"];
		ci.lap = v["piecePosition"]["lap"];
		
		ci.crashed_ticks = (g.car_last_crashed[idx] + g.respawn_ticks) - current_frame + 1;
		if (ci.crashed_ticks<0 || g.car_last_crashed[idx]==0) ci.crashed_ticks = 0;
		if (g.car_dnf[idx]) ci.crashed_ticks = 1000;
		
		ci.bumped = false;
		ci.front_bumped = false;

		ci.rot = 0;
		ci.speed = 0;
		ci.last_throttle = 0;
		ci.has_turbo = g.car_has_turbo[idx] && !ci.crashed_ticks;
		ci.turbo_ticks = (g.car_last_turbo[idx] + g.turbo_ticks) - current_frame;
		if (ci.turbo_ticks<0 || g.car_last_turbo[idx]==0) ci.turbo_ticks = 0; 
		if (g.car_last_turbo[idx]>current_frame) {
			ci.turbo_ticks = 0;
			ci.turbo_next_tick = true;
		} else ci.turbo_next_tick = false;

		ci.laps_finished = g.car_laps_finished[idx];
		if (g.r.laps && ci.laps_finished>=g.r.laps) ci.crashed_ticks = 1000;
	}

	return r;
}

race race_from_json(game<>&g,json_value&jv) {
	race r;

	r.laps = jv["raceSession"]["laps"];
	r.max_lap_time_ms = jv["raceSession"]["maxLapTimeMs"];
	r.quick_race = jv["raceSession"]["quickRace"];

	r.cars.resize(g.car_color_map.size());
	for (auto&v : jv["cars"].vector) {
		size_t idx = g.get_car_index(v["id"]["color"]);
		if (r.cars.size()<=idx) r.cars.resize(idx+1);
		auto&c = r.cars[idx];
		c.color = (a_string)v["id"]["color"];
		c.name = (a_string)v["id"]["name"];
		c.guide_flag_pos = v["dimensions"]["guideFlagPosition"];
		c.length = v["dimensions"]["length"];
		c.width = v["dimensions"]["width"];
	}

	r.track.id = (a_string)jv["track"]["id"];
	r.track.name = (a_string)jv["track"]["name"];
	for (auto&v : jv["track"]["lanes"].vector) {
		size_t index = (int)v["index"];
		if (r.track.lanes.size()<=index) r.track.lanes.resize(index+1);
		r.track.lanes[index] = v["distanceFromCenter"];
	}
	for (auto&v : jv["track"]["pieces"].vector) {
		r.track.pieces.emplace_back();
		auto&p = r.track.pieces.back();
		p.length = v["length"];
		p.angle = (double)v["angle"]/180*PI;
		p.radius = v["radius"];
		p.is_switch = v["switch"];
	}

	return r;
}

race race;

template<typename T>
T rng(T min,T max) {
	return tsc::rng(max-min) + min;
}


a_vector<state> all_states;
a_vector<double> all_throttles;

track_t::piece&get_piece(size_t index) {
	if (index>=race.track.pieces.size()) xcept("invalid piece index %d",index);
	return race.track.pieces[index];
};
auto get_lane = [&](size_t index) {
	if (index>=race.track.lanes.size()) xcept("invalid lane index %d\n",index);
	return race.track.lanes[index];
};
a_vector<double> lane_lengths;
double get_piece_length(size_t index,int start_lane,int end_lane) {
	int sw = 0;
	if (end_lane<start_lane) sw = -1;
	if (end_lane>start_lane) sw = 1;
	size_t idx = index*race.track.lanes.size()*3 + start_lane*3 + 1+sw;
	return lane_lengths.at(idx);
};


double get_center_piece_length(size_t index) {
	auto&p = get_piece(index);
	if (p.angle==0) return p.length;
	return std::abs(p.radius * p.angle);
};

a_map<std::tuple<double,double,double,double>,std::array<double,100>> switching_piece_radius_index_data;
a_vector<std::array<double,100>*> lane_switch_radiuses;
	

double get_piece_radius(size_t index,int start_lane,int end_lane,double piece_pos) {
	if (start_lane==end_lane) {
		double lanepos = get_lane(start_lane);
		auto&p = get_piece(index);
		return p.radius+(p.angle<0 ? lanepos : -lanepos);
	}
	size_t idx = index*race.track.lanes.size()*2 + start_lane*2 + (end_lane<start_lane ? 0 : 1);
	size_t idx2 = (size_t)(piece_pos / get_piece_length(index,start_lane,end_lane) * 100.0);
	if (idx2>=100) idx2 = 100;
	auto*arr = lane_switch_radiuses.at(idx);
	if (!arr) xcept("no radiuses for %d %d %d",index,start_lane,end_lane);
	return (*arr)[idx2];
}

double radius_from_three_points(double x1,double y1,double x2,double y2,double x3,double y3) {
	double x21 = x2-x1;
	double y21 = y2-y1;
	double x23 = x2-x3;
	double y23 = y2-y3;
	double x31 = x3-x1;
	double y31 = y3-y1;
	double u = sqrt((x21*x21 + y21*y21) * (x23*x23 + y23*y23)*(x31*x31 + y31*y31));
	double l = 2 * std::abs(x1*y2 + x2*y3 + x3*y1 - x1*y3 - x2*y1 - x3*y2);
	return u / l;
}

double cubic_bezier(double t,double a,double b,double c,double d) {
	double t2 = t * t;
	double t3 = t2 * t;
	double mt = (1-t);
	double mt2 = mt*mt;
	double mt3 = mt*mt*mt;
	return mt3*a + 3*mt2*t*b + 3*mt*t2*c + t3*d;
};

double quadratic_bezier(double t,double a,double b,double c) {
	double t2 = t * t;
	double mt = (1-t);
	double mt2 = mt*mt;
	return mt2*a + 2*mt*t*b + t2*c;
};

template<typename cb_F>
void walk_curved_switch(double from_radius,double to_radius,double angle,cb_F&&cb) {

	double start_x = 0.0;
	double start_y = 0.0;

	double end_x = 1.0*from_radius - cos(angle)*to_radius;
	double end_y = 0.0*from_radius - sin(angle)*to_radius;

	double mid_x = 2*(1.0*from_radius - cos(angle/2)*(from_radius+to_radius)/2) - start_x/2 - end_x/2;
	double mid_y = 2*(0.0*from_radius - sin(angle/2)*(from_radius+to_radius)/2) - start_y/2 - end_y/2;

	double step = 1.0/10000;
	for (double t=step;t<1.0+step;t+=step) {

		double x = -quadratic_bezier(t,start_x,mid_x,end_x);
		double y = -quadratic_bezier(t,start_y,mid_y,end_y);

		cb(x,y);

	}

}

template<typename cb_F>
void walk_straight_switch(double end_x,double end_y,cb_F&&cb) {

	double start_x = 0.0;
	double start_y = 0.0;

	double c1x = end_x*0.1;
	double c1y = end_y*0.25;
	double c2x = end_x*0.875;
	double c2y = end_y*0.75;

	double step = 1.0/10000;
	for (double t=step;t<1.0+step;t+=step) {

		double x = -cubic_bezier(t,0.0,c1x,c2x,end_x);
		double y = -cubic_bezier(t,0.0,c1y,c2y,end_y);

		cb(x,y);

	}

}


void calculate_track_values(game<>&g) {
	auto&track = g.r.track;

	lane_lengths.clear();
	lane_switch_radiuses.clear();
	lane_lengths.resize(track.pieces.size()*track.lanes.size()*3);
	lane_switch_radiuses.resize(track.pieces.size()*track.lanes.size()*3*2);

	for (size_t pidx=0;pidx<track.pieces.size();++pidx) {
		auto&p = track.pieces[pidx];
		for (size_t l=0;l<track.lanes.size();++l) {
			double*ptr = &lane_lengths[pidx*track.lanes.size()*3 + l*3];
			double&swl = ptr[0];
			double&nosw = ptr[1];
			double&swr = ptr[2];

			if (p.radius) {
				double lanepos = track.lanes[l];
				double radius = p.radius + (p.angle<0 ? lanepos : -lanepos);
				nosw = std::abs(radius * p.angle);
			} else {
				nosw = p.length;
			}

			if (!p.is_switch) {
				swl = 0;
				swr = 0;
			} else {
				double lanepos1 = track.lanes[l];
				double radius1 = p.radius + (p.angle<0 ? lanepos1 : -lanepos1);
				if (p.angle==0) radius1 = 0;
				auto set = [&](int sw) {
					double lanepos2 = track.lanes[l+sw];
					double radius2 = p.radius + (p.angle<0 ? lanepos2 : -lanepos2);
					if (p.angle==0) radius2 = 0;

					double length = 0.0;
					
					if (radius1==0) {

						double end_x = lanepos2-lanepos1;
						double end_y = p.length;

						double lx = 0.0, ly = 0.0;
						walk_straight_switch(end_x,end_y,[&](double x,double y) {
							double xx = x-lx;
							double yy = y-ly;
							length += sqrt(xx*xx+yy*yy);
							lx = x;
							ly = y;
						});
						log(log_level_info,"length for straight switch (%g %g) is %.100g\n",end_x,end_y,length);

					} else {

						double r1 = radius1;
						double r2 = radius2;
						double a = p.angle;

						double lx = 0.0, ly = 0.0;
						walk_curved_switch(r1,r2,a,[&](double x,double y) {
							double xx = x-lx;
							double yy = y-ly;
							length += sqrt(xx*xx+yy*yy);
							lx = x;
							ly = y;
						});
						log(log_level_info,"length for curved switch (%g %g %g) is %.100g\n",r1,r2,a/PI*180,length);


						auto i = switching_piece_radius_index_data.emplace(std::piecewise_construct,std::make_tuple(r1,r2,a,0.0),std::make_tuple());
						if (i.second) {

							lx = 0; ly = 0;
							double llx = 0, lly = 0;
							double lpx = 0, lpy = 0;
							double llpx = 0, llpy = 0;
							double piece_size = length / 100.0;
							size_t idx = 0;
							double accumulated_length = 0.0;
							walk_curved_switch(r1,r2,a,[&](double x,double y) {
								double xx = x-lx;
								double yy = y-ly;
								double l = sqrt(xx*xx+yy*yy);
								accumulated_length += l;
								if (accumulated_length>=piece_size && idx<99) {
									accumulated_length -= piece_size;

									double r = radius_from_three_points(llpx,llpy,lpx,lpy,x,y);
									log("r %.100g\n",r);
									i.first->second[idx] = r;

									++idx;

									llpx = lpx;
									llpy = lpy;
									lpx = x;
									lpy = y;
								}
								llx = lx;
								lly = ly;
								lx = x;
								ly = y;
							});
							double r = radius_from_three_points(llpx,llpy,lpx,lpy,llx,lly);
							log("r %.100g\n",r);
							i.first->second[idx] = r;
						}
						lane_switch_radiuses[pidx*track.lanes.size()*2 + l*2 + (sw==-1 ? 0 : 1)] = &i.first->second;

					}



					if (sw==-1) swl = length;
					if (sw==1) swr = length;
				};
				if (l!=0) {
					set(-1);
				} else swl = 0;
				if (l!=track.lanes.size()-1) {
					set(1);
				} else swr = 0;
			}
		}
	}

}



template<>
double game<0>::get_throttle(double desired_speed) {
	xcept("get_throttle");
	return 0.0;
}




#include "play.h"


}

