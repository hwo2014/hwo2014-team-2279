

namespace play {
;


template<typename T>
a_string tostr(T&&val) {
	a_string s = "{";
	bool first = true;
	for (auto&v : val) {
		if (!first) s += ", ";
		s += format("%f",v);
		first = false;
	}
	return s + "}";
}

size_t current_piece_index;


state st;
a_vector<state> all_states;
a_vector<double> all_throttles;

size_t my_car_index;


size_t break_state = 0;


double variable_acceleration = 0.2;
double variable_resistance = -0.02;

double acceleration(double throttle,double speed) {
	return variable_acceleration*throttle + variable_resistance*speed;
}
double throttle_from_acceleration(double acceleration,double speed) {
	return (acceleration - variable_resistance*speed) / variable_acceleration;
}

double default_variable_angle_acceleration = -0.00125;
double default_variable_angle_resistance = -0.1;

double variable_angle_acceleration = default_variable_angle_acceleration;
double variable_angle_resistance = default_variable_angle_resistance;

double angle_correction(double speed,double angle,double rot) {
	return variable_angle_acceleration*speed*angle + variable_angle_resistance*rot;
}

//double default_variable_slip = pow(sqrt(9.0/32)/180*PI,2.0);
double default_variable_slip = sqrt(9.0/32)/180*PI;
double default_variable_slip_weight = -0.3/180*PI;

double variable_slip = default_variable_slip;
double variable_slip_weight = default_variable_slip_weight;

double angle_slip(double speed,double radius) {
	return (variable_slip/sqrt(radius)*speed + variable_slip_weight)*speed;
}

a_vector<double> abs_piece_pos;
double get_abs_piece_pos(size_t piece_index,size_t lane) {
	return abs_piece_pos[piece_index*race.track.lanes.size() + lane];
}

a_vector<double> abs_center_piece_pos;
double get_abs_center_piece_pos(size_t piece_index) {
	return abs_center_piece_pos[piece_index];
}

double real_crash_angle = 60.0/180*PI;
double crash_angle = 60.0/180*PI;
//double crash_angle = 59.9/180*PI;

double largest_slip_error;

#include <lmmin.h>

#include "simul.h"

#include "opt2.h"

a_vector<std::tuple<double,double,double,double,double,double>> slip_history;

a_vector<std::reference_wrapper<double>> fit_values_refs;

bool values_fitted = false;

void fit_values() {

	values_fitted = true;

	fit_values_refs.clear();
	fit_values_refs.push_back(std::ref(variable_angle_acceleration));
	fit_values_refs.push_back(std::ref(variable_angle_resistance));
	fit_values_refs.push_back(std::ref(variable_slip));
	fit_values_refs.push_back(std::ref(variable_slip_weight));

	a_vector<double> parvec;
	for (double&v : fit_values_refs) {
		parvec.push_back(v);
	}
	a_vector<double> original_values;
	for (double&v : fit_values_refs) {
		original_values.push_back(v);
	}

	if (slip_history.size()<parvec.size()) return;

	lm_status_struct status;
	lm_control_struct control = lm_control_double;
	control.verbosity = 0;
	control.xtol = 1e-16;
	control.ftol = 1e-16;
	control.patience = 1000;

	lmmin(parvec.size(),parvec.data(),slip_history.size(),nullptr,[](const double*par,int m_dat,const void*data,double*fvec,int*userbreak) {

		for (size_t i=0;i<fit_values_refs.size();++i) {
			(double&)fit_values_refs[i] = par[i];
		}

		for (int i=0;i<m_dat;++i) {
			double rotacc, speed, angle, rot, radius, piece_angle;
			std::tie(rotacc,speed,angle,rot,radius,piece_angle) = slip_history[i];

			double s = angle_correction(speed,angle,rot);
			if (piece_angle<0) s -= angle_slip(speed,radius);
			else s += angle_slip(speed,radius);

			fvec[i] = (rotacc - s);
		}
		
	},&control,&status);

	for (size_t i=0;i<fit_values_refs.size();++i) {
		(double&)fit_values_refs[i] = parvec[i];
	}

	log(log_level_info,"fitted after %d iterations (fnorm %g): %s\n",status.nfev,status.fnorm,lm_infmsg[status.outcome]);

	log(log_level_info,"fitted parameters - ");
	for (auto&v : parvec) log(log_level_info,"%.100g ",v);
	log(log_level_info,"\n");

	log(log_level_info,"normalized variables - \n");
	log(log_level_info,"variable_angle_acceleration: %g\n",variable_angle_acceleration/default_variable_angle_acceleration);
	log(log_level_info,"variable_angle_resistance:   %g\n",variable_angle_resistance/default_variable_angle_resistance);
	log(log_level_info,"variable_slip:               %g\n",variable_slip/default_variable_slip);
	log(log_level_info,"variable_slip_weight:        %g\n",variable_slip_weight/default_variable_slip_weight);

	largest_slip_error = 0;
	for (auto&v : slip_history) {
		double rotacc, speed, angle, rot, radius, piece_angle;
		std::tie(rotacc,speed,angle,rot,radius,piece_angle) = v;

		double s = angle_correction(speed,angle,rot);
		if (piece_angle<0) s -= angle_slip(speed,radius);
		else s += angle_slip(speed,radius);

		double err = std::abs(rotacc - s);
		if (err>largest_slip_error) largest_slip_error = err;
	}
	if (largest_slip_error>1e-6) {
		log(log_level_info," warning: largest slip error is %.100g. this is a bug :(\n",largest_slip_error);

		log(log_level_info," warning: reducing history and resetting to original values\n");
		for (size_t i=0;i<fit_values_refs.size();++i) {
			(double&)fit_values_refs[i] = original_values[i];
		}
		//slip_history.clear();
		slip_history.resize(slip_history.size());
		largest_slip_error = 0.0;
	}
}


double last_rot;
double last_speed;

tsc::high_resolution_timer play_ht;

int last_speed_error;
int last_last_speed_error;


action play(game<>&g) {

	play_ht.reset();

	race = g.r;
	my_car_index = g.my_car_index;

	log("g.last_throttle is %f\n",g.last_throttle);

	st = g.st;
	all_states.push_back(g.st);
	all_throttles.push_back(g.last_throttle);

	//tsc::ut_impl::enter(main_ut);

	current_piece_index = g.st.cars.at(my_car_index).piece_index;
	{
		auto&car_pos = g.st.cars.at(my_car_index);
		log(" --- at piece %d %f (of %f) lane %d %d\n",current_piece_index,car_pos.piece_pos,get_piece_length(car_pos.piece_index,car_pos.start_lane,car_pos.end_lane),car_pos.start_lane,car_pos.end_lane);
	}
	if (g.st.cars.at(my_car_index).crashed_ticks) log(" (crashed)\n");
// 	if (all_states.size()>2) {
// 
// 		auto&car_pos = all_states.back().cars[my_car_index];
// 		auto&last_car_pos = all_states[all_states.size()-2].cars[my_car_index];
// 		double speed = car_pos.piece_pos - last_car_pos.piece_pos;
// 
// 		log("speed is maybe %f\n",speed);
// 	}

	double current_speed = 0;
	if (all_states.size()>3) {
		auto&car_pos = all_states.back().cars[my_car_index];
		auto&last_car_pos = all_states[all_states.size()-2].cars[my_car_index];

		double speed;
		if (car_pos.piece_index==last_car_pos.piece_index) speed = car_pos.piece_pos - last_car_pos.piece_pos;
		else speed = car_pos.piece_pos + get_piece_length(last_car_pos.piece_index,last_car_pos.start_lane,last_car_pos.end_lane)-last_car_pos.piece_pos;
		current_speed = speed;


		double last_throttle = g.last_throttle;
		//if (g.turbo_until>=current_frame-1) last_throttle *= g.turbo_factor;
		if (last_car_pos.turbo_ticks) last_throttle *= g.turbo_factor;
		double expected_speed = last_speed + acceleration(last_throttle,last_speed);
		if (car_pos.crashed_ticks) expected_speed = 0;
		log("last_throttle was %f\n",last_throttle);

		if (car_pos.turbo_ticks) log("turbo ticks: %d\n",car_pos.turbo_ticks);

		last_last_speed_error = last_speed_error;
		if (std::abs(speed-expected_speed)>1e-4) {
			if (current_frame>8) {
				if (last_car_pos.start_lane!=last_car_pos.end_lane && car_pos.start_lane==car_pos.end_lane) log(log_level_info,"unexpected speed after switch - speed is %f, expected_speed is %f\n",speed,expected_speed);
				else log(log_level_info,"unexpected speed - speed is %f, expected_speed is %f\n",speed,expected_speed);
			}
			last_speed_error = current_frame;
		} else current_speed = expected_speed;
		log("current speed is %f, angle %f\n",current_speed,car_pos.angle*180/PI);
	}

	if (all_states.size()>2) {
		
		auto&car_pos = all_states.back().cars[my_car_index];
		auto&last_car_pos = all_states[all_states.size()-2].cars[my_car_index];
		auto&last_last_car_pos = all_states[all_states.size()-3].cars[my_car_index];


		if (!values_fitted) {
			double fact = 1.0;

			static int ticks_near_a_curve = 0;

			double my_pos = get_abs_center_piece_pos(car_pos.piece_index) + car_pos.piece_pos/get_piece_length(car_pos.piece_index,car_pos.start_lane,car_pos.end_lane);
			bool is_near = false;
			for (size_t i=0;i<race.track.pieces.size();++i) {
				auto&p = race.track.pieces[i];
				if (!p.radius) continue;
				if (i==car_pos.piece_index || std::abs(my_pos - get_abs_center_piece_pos(i)<20.0)) {
					is_near = true;
					break;
				}

			}
			if (is_near) ++ticks_near_a_curve;

			fact = (200.0 / (std::max(ticks_near_a_curve-30,15)+30)) - 0.25;
			
			log(log_level_info,"fact %g\n",fact);
			variable_slip = default_variable_slip * fact;
			variable_slip_weight = default_variable_slip_weight * fact;
		}

// 		if (slip_history.size()>10) {
// 			fit_values();
// 		}

		//if (!car_pos.crashed && car_pos.piece_index==last_car_pos.piece_index && last_car_pos.piece_index==last_last_car_pos.piece_index) {
		if (!car_pos.crashed_ticks) {

			//double last_speed = last_car_pos.piece_pos - last_last_car_pos.piece_pos;
			double last_rot = last_car_pos.angle - last_last_car_pos.angle;
			double rot = car_pos.angle - last_car_pos.angle;
			double rotacc = rot-last_rot;

			auto&lp = get_piece(last_car_pos.piece_index);
			//double lanepos = get_lane(last_car_pos.start_lane);
			//double last_radius = lp.radius+(lp.angle<0 ? lanepos : -lanepos);
			double last_radius = 0.0;
			if (lp.radius!=0) last_radius = get_piece_radius(last_car_pos.piece_index,last_car_pos.start_lane,last_car_pos.end_lane,last_car_pos.piece_pos);

			double correct = angle_correction(last_speed,last_car_pos.angle,last_rot);

			log("last_speed %f angle %f last_rot %f rotacc %.20f correct %.20f radius %f\n",last_speed,last_car_pos.angle*180/PI,last_rot,rotacc,correct,last_radius);

			double slip = std::abs(rotacc-correct);

			log("actual slip %.100g\n",slip);
			log(log_level_info,"last_radius is %f\n",last_radius);

			//if (car_pos.piece_index==last_car_pos.piece_index && last_car_pos.piece_index==last_last_car_pos.piece_index) {
			if (true) {
				//if (car_pos.start_lane==car_pos.end_lane) {
					if (last_radius && slip>0.001 && current_frame-last_speed_error>=5) {
					//if (last_radius && slip>0.001 && current_frame-last_speed_error>=5) {

						if (slip_history.size()<0x20) slip_history.emplace_back(rotacc,last_speed,last_car_pos.angle,last_rot,last_radius,lp.angle);

						{
							double s = angle_correction(last_speed,last_car_pos.angle,last_rot);
							if (lp.angle<0) s -= angle_slip(last_speed,last_radius);
							else s += angle_slip(last_speed,last_radius);

							double diff = s - rotacc;

							if (std::abs(diff)>1e-8) {
								log(log_level_info,"diff is %.100g\n",diff);
								fit_values();
							}
						}
					}
				//}
			}

		}

	}

	{
		abs_piece_pos.resize(race.track.pieces.size() * race.track.lanes.size());
		for (size_t l=0;l<race.track.lanes.size();++l) {
			double pos = 0.0;
			for (size_t i=0;i<race.track.pieces.size();++i) {
				abs_piece_pos[i*race.track.lanes.size() + l] = pos;
				pos += get_piece_length(i,l,l);
			}
		}

		abs_center_piece_pos.resize(race.track.pieces.size());
		double pos = 0.0;
		for (size_t i=0;i<race.track.pieces.size();++i) {
			abs_center_piece_pos[i] = pos;
			pos += get_center_piece_length(i);
		}
	}
	if (all_states.size()>=8) {
		a_vector<std::tuple<size_t,size_t>> bumps;
		for (size_t i=0;i<st.cars.size();++i) {
			auto&car_pos = all_states[all_states.size()-1].cars[i];
			auto&last_car_pos = all_states[all_states.size()-2].cars[i];

			car_pos.rot = car_pos.angle - last_car_pos.angle;
			double moved;
			if (car_pos.piece_index==last_car_pos.piece_index) moved = car_pos.piece_pos - last_car_pos.piece_pos;
			else moved = car_pos.piece_pos + get_piece_length(last_car_pos.piece_index,last_car_pos.start_lane,last_car_pos.end_lane)-last_car_pos.piece_pos;
			if (all_states.size()==8) {
				car_pos.speed = moved;
				car_pos.last_throttle = 1.0;
			} else if (car_pos.crashed_ticks) {
				car_pos.speed = 0;
				car_pos.last_throttle = 0;
			} else {
				double max_throttle = last_car_pos.turbo_ticks ? g.turbo_factor : 1.0;
				double max = last_car_pos.speed+acceleration(max_throttle,last_car_pos.speed);
				log("%d moved %.100g (max is %.100g)\n",i,moved,max);

				bool bumped = false;
				bool front_bumped = false;
				double my_pos = get_abs_piece_pos(car_pos.piece_index,car_pos.start_lane) + car_pos.piece_pos;
				for (size_t i2=0;i2<st.cars.size();++i2) {
					if (i==i2) continue;
					auto&n_car_pos = all_states[all_states.size()-1].cars[i2];
					if (car_pos.start_lane!=n_car_pos.start_lane || car_pos.end_lane!=n_car_pos.end_lane) continue;
					double n_pos = get_abs_piece_pos(n_car_pos.piece_index,n_car_pos.start_lane) + n_car_pos.piece_pos;
					log("my_pos %f n_car_pos %f\n",my_pos,n_pos);
					if (std::abs(my_pos-n_pos-race.cars[i].length)<1e-6) {
						log("car %d got bumped by %d\n",i,i2);
						bumped = true;

						bumps.emplace_back(i,i2);
					}
					if (std::abs(n_pos-my_pos-race.cars[i].length)<1e-6) {
						log("car %d bumped into %d\n",i,i2);
						front_bumped = true;
					}
				}
				
				if (bumped) {
					// unfortunately, it's impossible to determine the throttle when someone gets bumped
					double throttle = last_car_pos.last_throttle;
					if (last_car_pos.turbo_ticks) last_car_pos.last_throttle *= g.turbo_factor;
					car_pos.speed = last_car_pos.speed + acceleration(throttle,last_car_pos.speed);
					car_pos.last_throttle = last_car_pos.last_throttle;
					log(log_level_info,"%d was bumped\n",i);
				} else {
					if (moved>=0 && moved<=1e-4+last_car_pos.speed+acceleration(max_throttle,last_car_pos.speed)) car_pos.speed = moved;
					else {
						car_pos.speed = moved;
						log(log_level_info," warning: car %d moved %f units, max is %f\n",i,moved,max);
					}
					car_pos.last_throttle = throttle_from_acceleration(car_pos.speed-last_car_pos.speed,last_car_pos.speed);
					if (std::abs(car_pos.last_throttle)<1e-8) car_pos.last_throttle = 0.0;

					if (car_pos.last_throttle<-1e-4 || car_pos.last_throttle>(last_car_pos.turbo_ticks ? 3.0 : 1.0)+1e-4) {
						log(log_level_info," warning: car %d last throttle was %g\n",i,car_pos.last_throttle);
						last_speed_error = current_frame;
					}
					double t = g.last_throttle;
					if (last_car_pos.turbo_ticks) t *= g.turbo_factor;
					if (i==my_car_index) log(log_level_info,"last throttle for %d is %g (actual %g)\n",i,car_pos.last_throttle,t);
					else log(log_level_info,"last throttle for %d is %g\n",i,car_pos.last_throttle);
				}

				car_pos.bumped = bumped;
				car_pos.front_bumped = front_bumped;
			}
			st.cars[i] = car_pos;
		}

		for (auto&v : bumps) {
			size_t a, b;
			std::tie(a,b) = v;

			auto&car_pos = all_states[all_states.size()-1].cars[a];
			auto&n_car_pos = all_states[all_states.size()-1].cars[b];

			double s = car_pos.speed;
			car_pos.speed = n_car_pos.speed * 0.9;
			n_car_pos.speed = s * 0.8;

			st.cars[a] = car_pos;
			st.cars[b] = n_car_pos;

		}

	}


	if (all_states.size()==3) {
		auto&st0 = all_states[0].cars[my_car_index];
		auto&st1 = all_states[1].cars[my_car_index];
		auto&st2 = all_states[2].cars[my_car_index];
		double pos0, pos1, pos2;
		pos0 = st0.piece_pos;
		pos1 = st1.piece_pos;
		if (st0.piece_index!=st1.piece_index) pos1 += get_piece_length(st0.piece_index,st0.start_lane,st0.end_lane);
		pos2 = st2.piece_pos;
		if (st0.piece_index!=st2.piece_index) pos2 += get_piece_length(st0.piece_index,st0.start_lane,st0.end_lane);
		if (st1.piece_index!=st2.piece_index) pos2 += get_piece_length(st1.piece_index,st1.start_lane,st1.end_lane);
		double a0x = pos1 - pos0;
		double a0  = a0x / all_throttles[1];
		double a1x = (pos2 - pos1) - a0x;
		double a1 = a1x / all_throttles[2];
		double acc = a0;
		log("acc is %f\n",acc);
		log("a0 is %f, a1 is %f\n",a0,a1);
		double res = (a0x-a1x) / a0x;
		log("res is %f\n",res);

		log(log_level_info,"acceleration is %.20f, resistance is %.20f\n",acc,res);

 		variable_acceleration = acc;
 		variable_resistance = -res;

	}


	action a;

	double opt_time = 0;
	{
		double t = play_ht.elapsed();
		opt2::turbo_factor = g.turbo_factor;
		opt2::turbo_ticks = g.turbo_ticks;
		opt2::my_last_switch = g.last_switch;
		opt2::turbo_available_in = g.next_turbo_available - current_frame;
		a = opt2::test();
		//a = action(action::t_throttle,tsc::rng(1.0));
		opt_time = play_ht.elapsed() - t;

		if (all_states.size()<8) a = action(action::t_throttle,1.0);

		auto&car_pos = st.cars[my_car_index];
		if (a.t==action::t_turbo && !car_pos.has_turbo) xcept("attempt to use unavailable turbo");

		if (a.t==action::t_switch) {
			log(log_level_info,"action switch %g\n",a.v);
		}
	}
	
	
	last_speed = current_speed;

	//tsc::ut_impl::leave(main_ut);

	log("play took %fms -- %fms opt\n",play_ht.elapsed()*1000,opt_time*1000);
	
	return a;
}

void finish() {

	//tsc::ut_impl::enter(main_ut);

	xcept("finished");
	//opt::test();
	xcept("stop");


	//tsc::ut_impl::leave(main_ut);




	xcept("stop");

}


}

