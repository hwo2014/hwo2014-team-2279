
a_vector<size_t> fast_lane_for_piece;

void calculate_fast_lanes() {

	fast_lane_for_piece.resize(race.track.pieces.size());

	a_set<std::tuple<size_t,size_t>> visited;
	typedef std::tuple<double,size_t,size_t,size_t,size_t> open_t;
	std::priority_queue<open_t,a_vector<open_t>> open;
	a_deque<open_t> closed;
	
	for (size_t i=0;i<race.track.lanes.size();++i) {
		open.emplace(0.0,0,i,i,0);
		visited.emplace(0,i);
	}
	size_t end = 0;
	while (!open.empty()) {
		size_t pidx, lane, start_lane, prev;
		double neg_distance;
		std::tie(neg_distance,pidx,lane,start_lane,prev) = open.top();
		open.pop();
		size_t cidx = closed.size();
		closed.emplace_back(neg_distance,pidx,lane,start_lane,prev);

		if (pidx==race.track.pieces.size()) {
			if (lane!=start_lane) continue;
			end = prev;
			break;
		}

		auto&p = get_piece(pidx);
		//if (p.is_switch && !p.angle) {
		if (p.is_switch) {
			for (int i=-1;i<=1;++i) {
				if (lane==0 && i==-1) continue;
				if (lane==race.track.lanes.size()-1 && i==1) continue;
				if (!visited.emplace(pidx+1,lane+i).second) continue;
				//open.emplace(neg_distance - rng(0.0,1.0),pidx+1,size_t(lane+i),start_lane,cidx);
				open.emplace(neg_distance - get_piece_length(pidx,lane,lane+i),pidx+1,size_t(lane+i),start_lane,cidx);
			}
		} else {
			//open.emplace(neg_distance - rng(0.0,1.0),pidx+1,lane,start_lane,cidx);
			open.emplace(neg_distance - get_piece_length(pidx,lane,lane),pidx+1,lane,start_lane,cidx);
		}

	}

	for (size_t i=end;;) {
		size_t pidx, lane, start_lane, prev;
		double neg_distance;
		std::tie(neg_distance,pidx,lane,start_lane,prev) = closed[i];

		log("lane for piece %d is %d with distance %f\n",pidx,lane,-neg_distance);
		if (get_piece(pidx).is_switch) log(" (switch)\n");
		fast_lane_for_piece[pidx] = lane;

		if (i==prev) break;
		i = prev;
	}


}
