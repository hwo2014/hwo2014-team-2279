
struct simul {

	struct state {

		size_t piece_index;
		double piece_pos;
		double speed;
		double rot;
		double angle;
		size_t start_lane, end_lane;
		double rotacc;

		double throttle;

		int last_switch;
		int turbo_ticks;
		bool has_turbo;
		bool turbo_next_tick;

		int tick;
		int crashed_ticks;

		double piece_abs_distance;
		int laps;
		int laps_finished, lap;
		int next_desired_lane;
		//int bump_idx;
		tsc::bitset<16*32> bumps, survived_bumps, bumps2;
		//state() : piece_abs_distance(0), laps(0), bump_idx(-1) {}
		state() : piece_abs_distance(0), laps(0) {}
	};


	size_t lanes_n;
	size_t pieces_n;
	a_vector<double> piece_lengths, piece_angles, piece_radiuses;
	a_vector<double> lanes;

	int turbo_ticks;
	double turbo_factor;

	simul() {
		lanes_n = race.track.lanes.size();
		pieces_n = race.track.pieces.size();
		for (size_t i=0;i<pieces_n;++i) {
			for (size_t l=0;l<lanes_n;++l) {
				//if (get_piece(i).angle==0 && get_piece(i).length==100) {
					piece_lengths.push_back(get_piece_length(i,l,l?l-1:l));
					piece_lengths.push_back(get_piece_length(i,l,l));
					piece_lengths.push_back(get_piece_length(i,l,l<lanes_n-1?l+1:l));
// 				} else {
// 					piece_lengths.push_back(get_piece_length(i,l,l));
// 					piece_lengths.push_back(get_piece_length(i,l,l));
// 					piece_lengths.push_back(get_piece_length(i,l,l));
// 				}
			}
			auto&p = get_piece(i);
			piece_angles.push_back(p.angle);
			piece_radiuses.push_back(p.radius);
		}
		for (size_t i=0;i<lanes_n;++i) {
			lanes.push_back(get_lane(i));
		}
	}

	void advance(state&st,action a) {
		advance(st,a,[](state&st){});
	}
	template<typename bumps_F>
	void advance(state&st,action a,bumps_F&&do_bumps) {

		int sw = 0;
		if (st.start_lane!=st.end_lane) {
			if (st.start_lane<st.end_lane) sw = 1;
			else sw = -1;
		}

		if (st.turbo_next_tick) {
			st.turbo_ticks = turbo_ticks;
			st.turbo_next_tick = false;
		}
		
		if (a.t==action::t_throttle) {
			st.throttle = a.v;
		} else if (a.t==action::t_switch) {
			st.last_switch = (int)a.v;
			//log("simul: action switch %d at %d\n",st.last_switch,st.piece_index);
		} else if (a.t==action::t_turbo) {
			st.has_turbo = false;
			st.turbo_next_tick = true;
		}
		else xcept("simul: unknown action");

 		double throttle = st.throttle;
		if (st.turbo_ticks) {
			throttle *= turbo_factor;
			--st.turbo_ticks;
		}

		double acc = acceleration(throttle,st.speed);

		double radius = 0;
		if (piece_angles[st.piece_index]) {
			double lanepos = lanes[st.start_lane];
			radius = piece_radiuses[st.piece_index]+(piece_angles[st.piece_index]<0 ? lanepos : -lanepos);
		}
		if (st.start_lane!=st.end_lane && piece_radiuses[st.piece_index]) {
			radius = get_piece_radius(st.piece_index,st.start_lane,st.end_lane,st.piece_pos);
		}

		double correct = angle_correction(st.speed,st.angle,st.rot);
		//double correct = -(0.00125*st.speed)*st.angle - 0.1*st.rot;
		//double slip = values.angle_slip(st.speed,radius) * 1.1;
		//double slip = angle_slip(st.speed,radius);
		//if (slip>0) slip += max_rot_offset;
		//if (slip>0 && st.turbo_ticks) slip += max_rot_offset*st.turbo_factor;
		//if (st.start_lane!=st.end_lane) slip *= 1.5;
		//slip *= 1.1;

		double rotacc = correct;
		if (radius) {
			double slip = angle_slip(st.speed,radius);
			if (slip>0) {
				slip += largest_slip_error;
				if (piece_angles[st.piece_index]<0) rotacc -= slip;
				else rotacc += slip;
			}
		}
		
		st.rotacc = rotacc;

		st.rot += rotacc;
		st.angle += st.rot;
		double max_angle = crash_angle;
		if (std::abs(st.angle)>=max_angle) {
			st.crashed_ticks = 400 + 1;
		}

		if (st.crashed_ticks) {
			--st.crashed_ticks;
			st.speed = 0;
			st.angle = 0;
			st.rot = 0;
			st.throttle = 0;
			st.last_switch = 0;
		} else {
			st.speed += acc;
			st.piece_pos += st.speed;
			auto piece_transitions = [&]() {
				while (st.piece_pos>=piece_lengths[st.piece_index*lanes_n*3 + st.start_lane*3+1+sw]) {
					st.piece_abs_distance += get_center_piece_length(st.piece_index);
					st.piece_pos -= piece_lengths[st.piece_index*lanes_n*3 + st.start_lane*3+1+sw];
					sw = 0;
					++st.piece_index;
					if (st.piece_index==pieces_n) st.piece_index = 0;

					st.start_lane = st.end_lane;
					if (st.last_switch && get_piece(st.piece_index).is_switch) {
						int nl = (int)st.start_lane + st.last_switch;
						//if (nl<0 || nl>=(int)lanes_n) xcept("bad switch from lane %d to lane %d at piece %d",st.start_lane,nl,st.piece_index);
						if (nl<0 || nl>=(int)lanes_n) {
							// this can theoretically happen since crashes and bumps will push the switch action around
							log(log_level_info," !!ERROR : bad switch from lane %d to lane %d at piece %d\n",st.start_lane,nl,st.piece_index);
							st.last_switch = 0;
						} else {
							st.end_lane = st.start_lane + st.last_switch;
							st.last_switch = 0;
						}
						//log("simul: switched from lane %d to lane %d at piece %d\n",st.start_lane,st.end_lane,st.piece_index);
					}
				}
			};
			piece_transitions();
			do_bumps(st);
			piece_transitions();
		}

		++st.tick;

	}

};





